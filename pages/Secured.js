import React, { Component } from "react";
import Menu from "./Menu";
import OccupancyInfoPage from "./OccupancyInfoPage";
import NfcPage from "./NfcPage";
import AccountPage from "./AccountPage";
import { View } from "react-native";
import { NativeRouter, Switch, Route } from "react-router-native";
import WristletInfo from "./WristletInfo";
import RentPage from "./RentPage";
import NotificationPage from "./NotificationPage";
import StorePage from "./StorePage";
import ToolbarNavigate from "./components/ToolbarNavigate";
import LockerOpen from "./LockerOpen";
import TicketInfo from "./TicketInfo";
import TicketCameraInfo from "./TicketCameraInfo";
import EAktywniOccupancyInfo from "./EAktywniOccupancyInfo";
import Epos from "./Epos";
import BasketPage from "./BasketPage";
import FinalizePage from "./FinalizePage";
import Tickets from "./Tickets";
import ZoneInfo from "./ZoneInfo";

class Secured extends Component {
  // apiUrl = this.props.apiUrl;

  getOperatorLoginIds = () => {
    return `${this.props.operatorLogin.objectDb},${this.props.operatorLogin.objectId}`
  };

  render() {
    return (
      <NativeRouter>
        <ToolbarNavigate />
        <View>
          <Switch>
            <Route
              path="/"
              exact
              render={() => (
                <Menu
                  operator={this.props.operatorLogin.name}
                  logout={this.props.onLogoutPress}
                />
              )}
            />
            <Route path="/occupancyInfo" render={() => <OccupancyInfoPage />} />
            <Route path="/nfc/:page" component={NfcPage} />
            <Route
              path="/accountInfo/:trhId"
              render={matchProps => <AccountPage {...matchProps} />}
            />
            <Route
              path="/wristlet/:trhId"
              render={matchProps => <WristletInfo {...matchProps} />}
            />
            <Route
              path="/rent"
              render={matchProps => <RentPage {...matchProps} />}
            />
            <Route
              path="/rent/:trhId"
              render={matchProps => <RentPage {...matchProps} />}
            />
            <Route
              path="/ticketInfo/:trhId"
              render={matchProps => <TicketInfo {...matchProps} operIds={this.getOperatorLoginIds()}/>}
            />
            <Route path="/notifications" render={() => <NotificationPage />} />
            <Route path="/store" render={() => <StorePage />} />
            <Route path="/open/lockers" render={() => <LockerOpen />} />
            <Route path="/camera/ticket/info" render={() => <TicketCameraInfo operIds={this.getOperatorLoginIds()}/>} />
            <Route path="/eaktywni/occupancy/info" render={() => <EAktywniOccupancyInfo />} />
            <Route path="/epos" render={() => <Epos />} />
            <Route path="/basket" render={() => <BasketPage />} />
            <Route path="/finalize" render={(matchProps) => <FinalizePage {...matchProps}  />} />
            <Route path="/tickets" render={() => <Tickets />} />
            <Route path="/zoneInfo" render={() => <ZoneInfo />} />
          </Switch>
          {/* <View style={{ margin: 20 }} /> */}
          {/* <Button onPress={this.props.onLogoutPress} title="Wyloguj" /> */}
        </View>
      </NativeRouter>
    );
  }
}

export default Secured;
