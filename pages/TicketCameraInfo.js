import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    AsyncStorage,
    TextInput,
    Keyboard,
    PermissionsAndroid,
    Platform, Button, Image, ScrollView
} from "react-native";
import { CameraKitCameraScreen, } from 'react-native-camera-kit';

class TicketCameraInfo extends Component {
    state = {
        ticketInfo: "",
        trhId: "",
        error: "",
        API_URL: "",
        deviceName: "",
        QR_Code_Value: '',
        Start_Scanner: false,
    };

    componentDidMount = () => {
        this.open_QR_Code_Scanner();
        AsyncStorage.getItem("apiUrl").then(apiUrl => {
            this.setState({ API_URL: apiUrl });

            AsyncStorage.getItem("deviceName").then(deviceName => {
                this.setState({ deviceName });
            });
            // let trhIdString = this.props.match.params.trhId;
            // trhId = trhIdString.replace("-", "/");
            // trhId = trhId.replace("-", "/");
            // this.checkSimpleEntry(trhId, apiUrl);
        });
    };

    checkSimpleEntry = (trhId, api) => {
        AsyncStorage.getItem("deviceName").then(deviceName => {
            const params = {
                ip: deviceName,
                trhId: trhId,
                operIds: this.props.operIds
            };
            const url = `${api}/EAktywni/entry/simple`;
            fetch(url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'X-Auth-App-Token': deviceName
                },
                body: JSON.stringify(params)
            })
                .then(response => {
                    if (response.ok) {
                        console.log(response.ok);
                        return response;
                    }
                    throw Error("Nie udało się");
                })
                .then(response => response.json())
                .then(data => {
                    if (!data.isError) {
                        this.setState({
                            ticketInfo: data.result,
                            trhId: "",
                            error: ""
                        });
                    } else {
                        this.setState({
                            ticketInfo: data.result,
                            trhId: "",
                            error: data.errorTable[0]
                        });
                    }
                })
                .catch(err => {
                    console.log(err);
                });
        });
    };

    onSubmitCheckSimplyEntry = trhId => {
        this.checkSimpleEntry(trhId, this.state.API_URL);
    };

    clearQRCode = () => {
        this.setState({
            QR_Code_Value: ""
        });
        this.open_QR_Code_Scanner();
    }

    render() {
        let dateTo = new Date();
        if (this.state.ticketInfo) {
            dateTo = new Date(this.state.ticketInfo.validto);
            dateTo = dateTo.toISOString().substring(0, 10);
        } else {
            dateTo = "";
        }

        if (!this.state.QR_Code_Value) {
            return (
                <>
                    <View style={{ flex: 1, marginLeft: "-50%"}}>

                        <CameraKitCameraScreen
                            showFrame={false}
                            scanBarcode={true}
                            laserColor={'#FF3D00'}
                            frameColor={'#00C853'}
                            colorForScannerFrame={'black'}
                            onReadCode={event =>
                                this.onQR_Code_Scan_Done(event.nativeEvent.codeStringValue)
                            }
                        />

                    </View>
                </>
            );
        } else {
            return (
                <View
                    style={[
                        this.state.error
                            ? styles.containerWristletInfoError
                            : styles.containerWristletInfo
                    ]}
                >
                    <ScrollView>
                        <Image
                            style={styles.ticketImg}
                            source={{uri: this.state.ticketInfo.pictureUrl}}
                        />

                        <View style={{marginTop: "65%"}}>
                            {!!this.state.error && (
                                <Text style={styles.wristletInfoText}>
                                    <Text style={styles.errorText}>{this.state.error}</Text>
                                </Text>
                            )}
                            <Text style={styles.wristletInfoText}>
                                Bilet:{" "}
                                <Text style={{ fontWeight: "bold", fontSize: 25 }}>
                                    {this.state.ticketInfo.name}
                                </Text>
                            </Text>
                            <Text style={styles.wristletInfoText}>
                                Pozostało wejść:{" "}
                                <Text style={{ fontWeight: "bold", fontSize: 25 }}>
                                    {this.state.ticketInfo.entrances}
                                </Text>
                            </Text>
                            <Text style={styles.wristletInfoText}>
                                Ważny do:{" "}
                                <Text style={{ fontWeight: "bold", fontSize: 25 }}>{dateTo}</Text>
                            </Text>
                            {this.state.ticketInfo !== "" && (
                                <Text style={styles.wristletInfoText}>
                                    {this.state.ticketInfo.passHistory.map((text, index) => (
                                        <Text key={index} style={{ fontSize: 20 }}>{text}</Text>
                                    ))}
                                </Text>
                            )}

                        </View>

                        <View style={styles.btnReturn}>
                            <Button
                                title="Skanuj"
                                color="#0AB6CE"
                                onPress={() => this.clearQRCode()}
                            />
                        </View>
                    </ScrollView>

                    <TextInput
                        style={{ color: "transparent" }}
                        onChangeText={trhId => this.onSubmitCheckSimplyEntry(trhId)}
                        autoFocus={true}
                        caretHidden={true}
                        showSoftInputOnFocus={false}
                        onFocus={Keyboard.dismiss}
                        value={this.state.trhId}
                    />
                </View>
            );
        }
    }

    onQR_Code_Scan_Done = (QR_Code) => {
        this.setState({ QR_Code_Value: QR_Code });
        this.setState({ Start_Scanner: false });
        this.onSubmitCheckSimplyEntry(QR_Code);
    }

    open_QR_Code_Scanner=()=> {
        var that = this;
        if (Platform.OS === 'android') {
            async function requestCameraPermission() {
                try {
                    const granted = await PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.CAMERA, {
                            'title': 'Camera App Permission',
                            'message': 'Camera App needs access to your camera '
                        }
                    )
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {

                        that.setState({ QR_Code_Value: '' });
                        that.setState({ Start_Scanner: true });
                    } else {
                        alert("CAMERA permission denied");
                    }
                } catch (err) {
                    alert("Camera permission err", err);
                    console.warn(err);
                }
            }
            requestCameraPermission();
        } else {
            that.setState({ QR_Code_Value: '' });
            that.setState({ Start_Scanner: true });
        }
    }
}

export default TicketCameraInfo;

const styles = StyleSheet.create({
    containerWristletInfo: {
        marginTop: "16%",
        // padding: 40,
        height: "95%",
        width: "100%",
        backgroundColor: "green"
    },
    containerWristletInfoError: {
        marginTop: "16%",
        // padding: 40,
        height: "95%",
        width: "100%",
        backgroundColor: "red"
    },
    errorText: {
        color: "white",
        fontSize: 25
    },
    wristletInfoText: {
        padding: 5,
        color: "#fff",
        fontSize: 25
    },
    btnDetails: {
        marginTop: 50,
        fontSize: 20
    },

    containerDetails: {
        flex: 1,
        marginTop: "20%",
        justifyContent: "center",
        alignItems: "center"
    },
    h2text: {
        marginTop: 10,
        fontFamily: "Helvetica",
        fontSize: 40,
        fontWeight: "bold",
        color: "#fff"
    },
    flatview: {
        justifyContent: "center",
        paddingTop: 30,
        borderRadius: 2
    },
    name: {
        fontFamily: "Verdana",
        fontSize: 23,
        color: "#fff"
    },
    valid: {
        color: "green",
        fontSize: 23
    },
    btnReturn: {
        marginBottom: 30
    },
    ticketImg: {
        position: "absolute",
        width: "100%",
        height: "30%",
        top: 0,
        left: 0
    },
});
