import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  FlatList,
  AsyncStorage
} from "react-native";

class OccupancyInfoPage extends Component {
  state = {
    occupancy: "0",
    zoneList: [],
    API_URL: ""
  };

  componentDidMount = () => {
    AsyncStorage.getItem("apiUrl").then(apiUrl => {
      this.setState({ API_URL: apiUrl });
      this.getOccupancyInfo(apiUrl);
      this.getZoneOccupancyInfo(apiUrl);
    });
  };

  getOccupancyInfo = apiUrl => {
    const url = `${apiUrl}/occupancyInfo`;
    fetch(url)
      .then(response => {
        if (response.ok) {
          return response;
        }
        throw Error("Nie udało się");
      })
      .then(response => response.json())
      .then(data => {
        if (!data.isError) {
          this.setState({
            occupancy: data.result.amount
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  getZoneOccupancyInfo = apiUrl => {
    const url = `${apiUrl}/crud/zone/occupancy`;
    fetch(url)
      .then(response => {
        if (response.ok) {
          return response;
        }
        throw Error("Nie udało się");
      })
      .then(response => response.json())
      .then(data => {
        if (!data.isError) {
          this.setState({
            zoneList: data.result
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    return (
      <ScrollView>
        <View style={styles.containerOccupancy}>
          <View>
            <Text style={styles.occupancyInfoText}>
              Ilość osób na obiekcie:{" "}
              <Text style={styles.occupancyInfoTextNumber}>
                {this.state.occupancy}
              </Text>
            </Text>
          </View>

          <View style={styles.zoneContainer}>
            <FlatList
              data={this.state.zoneList}
              renderItem={({ item }) => (
                <>
                  <View style={styles.zone}>
                    <Text style={[styles.zoneText, { width: "70%" }]}>
                      {item.name}{" "}
                    </Text>
                    <Text style={[styles.zoneText, { width: "20%" }]}>
                      {item.qnt}
                    </Text>
                  </View>
                  <View style={styles.hr} />
                </>
              )}
              keyExtractor={item => `${item.objectId}o`}
            />
          </View>
        </View>
        <Text>{this.state.message}</Text>
      </ScrollView>
    );
  }
}

export default OccupancyInfoPage;

const styles = StyleSheet.create({
  containerOccupancy: {
    flex: 1,
    marginTop: 100
  },
  occupancyInfoText: {
    fontSize: 26,
    color: "#fff"
  },
  occupancyInfoTextNumber: {
    fontWeight: "bold",
    color: "#f42363"
  },
  zoneContainer: {
    marginTop: 20
  },
  zone: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  hr: {
    marginLeft: "20%",
    borderBottomWidth: 1,
    borderBottomColor: "#f42363",
    width: "50%"
  },
  zoneText: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold"
  }
});
