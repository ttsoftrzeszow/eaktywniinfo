import React, { Component } from "react";
import {
  View,
  StyleSheet, AsyncStorage, ScrollView, Alert, Text, FlatList
} from "react-native";
import DatePicker from 'react-native-datepicker';

class Tickets extends Component {
  state = {
    API_URL: "",
    deviceName: "",
    ticketList: [],
    dateFrom: ""
  };

  componentDidMount = () => {
    AsyncStorage.getItem("apiUrl").then(apiUrl => {
      this.setState({ API_URL: apiUrl });
      AsyncStorage.getItem("deviceName").then(deviceName => {
        this.setState({
          deviceName,
          dateFrom: new Date().toISOString().substring(0,10)
        });
        this.getTicketList();
      });
    });
  };

  selectDate = (date) => {
    this.setState({dateFrom: date});
    this.getTicketList();
  }


  render() {
    return (
      <>
        <ScrollView >
          <View style={styles.container}>
            <DatePicker
              style={{width: 200}}
              date={this.state.dateFrom}
              mode="date"
              placeholder="Wybierz date"
              format="YYYY-MM-DD"
              confirmBtnText="Ok"
              cancelBtnText="Anuluj"
              androidMode="calendar"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0
                },
                dateInput: {
                  marginLeft: 36,
                },
                dateText: {
                  color: "#fff"
                }
              }}
              onDateChange={(date) => {this.selectDate(date)}}
            />

            <View style={styles.containerTables}>
              <FlatList
                data={this.state.ticketList}
                renderItem={({ item }) => (
                  <>
                    <View style={styles.row}>
                      <View style={{ width: "100%" }}>
                        <Text style={[styles.text, { width: "100%" }]}>
                          {item.date}
                        </Text>
                        <Text style={[styles.text, { width: "100%" }]}>
                          {item.name}
                        </Text>
                        <Text style={[styles.text, { width: "100%" }]}>
                          {item.ticketName}
                        </Text>
                      </View>
                      <View style={{ width: "100%" }}>
                        <Text style={[styles.text, { width: "100%" }]}>
                          Ilość: {item.qnt}
                        </Text>
                      </View>
                    </View>
                    <View style={styles.hr} />
                  </>
                )}
                keyExtractor={(item, index) => `${item.name}${index}`}
              />
            </View>
          </View>
        </ScrollView>

      </>
    );
  }


  getTicketList = () => {
    const url = `${this.state.API_URL}/EAktywni/admin/bought/passes/day?date=${this.state.dateFrom}&operToken=${this.state.deviceName}`;
    fetch(url, {
      headers: {
        'X-Auth-App-Token': this.state.deviceName
      }
    })
      .then(response => {
        if (response.ok) {
          return response;
        } else {
          Alert.alert("Wystąpił wyjątek!", "Brak połącznia z siecią");
        }
      })
      .then(response => response.json())
      .then(data => {
        if (!data.isError) {
          this.setState({
            ticketList: data.result
          });
        } else {
          if (data.errorTable.length > 0) {
            Alert.alert("Wystąpił wyjątek!", `${data.errorTable[0]}`);
          } else {
            Alert.alert("Wystąpił wyjątek!", "Nieznany błąd!");
          }
        }
      })
      .catch(err => {
        console.log(err);
        Alert.alert("Wystąpił wyjątek!", `${err}`);
      });
  }


}

export default Tickets;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 90,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    flexWrap: "wrap",
  },
  containerTables: {
    width: "95%",
    marginTop: 20
  },
  row: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  hr: {
    borderBottomWidth: 1,
    borderBottomColor: "#f42363",
    width: "100%"
  },
  text: {
    color: "#fff",
    textAlign: "center",
    padding: 5,
    fontSize: 15
  },
});
