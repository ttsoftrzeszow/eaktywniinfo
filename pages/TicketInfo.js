import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,
  TextInput,
  Keyboard, Image, ScrollView
} from "react-native";

class TicketInfo extends Component {
  state = {
    ticketInfo: "",
    trhId: "",
    error: "",
    API_URL: ""
  };

  componentDidMount = () => {
    AsyncStorage.getItem("apiUrl").then(apiUrl => {
      this.setState({ API_URL: apiUrl });
      let trhIdString = this.props.match.params.trhId;
      trhId = trhIdString.replace("-", "/");
      trhId = trhId.replace("-", "/");
      this.checkSimpleEntry(trhId, apiUrl);
    });
  };

  checkSimpleEntry = (trhId, api) => {
    AsyncStorage.getItem("deviceName").then(deviceName => {
      const params = {
        ip: deviceName,
        trhId: trhId,
        operIds: this.props.operIds
      };
      const url = `${api}/EAktywni/entry/simple`;
      fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          'X-Auth-App-Token': deviceName
        },
        body: JSON.stringify(params)
      })
        .then(response => {
          if (response.ok) {
            console.log(response.ok);
            return response;
          }
          throw Error("Nie udało się");
        })
        .then(response => response.json())
        .then(data => {
          console.log(data);
          if (!data.isError) {
            this.setState({
              ticketInfo: data.result,
              trhId: "",
              error: ""
            });
          } else {
            this.setState({
              ticketInfo: data.result,
              trhId: "",
              error: data.errorTable[0]
            });
          }
        })
        .catch(err => {
          console.log(err);
        });
    });
  };

  onSubmitCheckSimplyEntry = trhId => {
    this.checkSimpleEntry(trhId, this.state.API_URL);
  };

  render() {
    let dateTo = new Date();
    if (this.state.ticketInfo) {
      dateTo = new Date(this.state.ticketInfo.validto);
      dateTo = dateTo.toISOString().substring(0, 10);
    } else {
      dateTo = "";
    }

    return (
      <>
        <View
          style={[
            this.state.error
              ? styles.containerWristletInfoError
              : styles.containerWristletInfo
          ]}
        >
          <ScrollView>
            <Image
                style={styles.ticketImg}
                source={{uri: this.state.ticketInfo.pictureUrl}}
            />

            <View style={{marginTop: "65%"}}>
              {!!this.state.error && (
                  <Text style={styles.wristletInfoText}>
                    <Text style={styles.errorText}>{this.state.error}</Text>
                  </Text>
              )}
              <Text style={styles.wristletInfoText}>
                Bilet:{" "}
                <Text style={{ fontWeight: "bold", fontSize: 25 }}>
                  {this.state.ticketInfo.name}
                </Text>
              </Text>
              <Text style={styles.wristletInfoText}>
                Pozostało wejść:{" "}
                <Text style={{ fontWeight: "bold", fontSize: 25 }}>
                  {this.state.ticketInfo.entrances}
                </Text>
              </Text>
              <Text style={styles.wristletInfoText}>
                Ważny do:{" "}
                <Text style={{ fontWeight: "bold", fontSize: 25 }}>{dateTo}</Text>
              </Text>
              {this.state.ticketInfo !== "" && (
                  <Text style={styles.wristletInfoText}>
                    {this.state.ticketInfo.passHistory.map((text, index) => (
                        <Text key={index} style={{ fontSize: 20 }}>{text}</Text>
                    ))}
                  </Text>
              )}

            </View>

            {/*<View style={styles.btnReturn}>*/}
              {/* <Button
                title="Powrót"
                color="#0AB6CE"
                onPress={this.handleShowHideDetails}
              /> */}
            {/*</View>*/}
          </ScrollView>
          <TextInput
            style={{ color: "transparent" }}
            onChangeText={trhId => this.onSubmitCheckSimplyEntry(trhId)}
            autoFocus={true}
            caretHidden={true}
            showSoftInputOnFocus={false}
            onFocus={Keyboard.dismiss}
            value={this.state.trhId}
          />
        </View>
      </>
    );
  }
}

export default TicketInfo;

const styles = StyleSheet.create({
  containerWristletInfo: {
    marginTop: "16%",
    // padding: 40,
    height: "95%",
    width: "100%",
    backgroundColor: "green"
  },
  containerWristletInfoError: {
    marginTop: "16%",
    // padding: 40,
    height: "95%",
    width: "100%",
    backgroundColor: "red"
  },
  errorText: {
    color: "white",
    fontSize: 25
  },
  wristletInfoText: {
    padding: 5,
    color: "#fff",
    fontSize: 25
  },
  btnDetails: {
    marginTop: 50,
    fontSize: 20
  },

  containerDetails: {
    flex: 1,
    marginTop: "20%",
    justifyContent: "center",
    alignItems: "center"
  },
  h2text: {
    marginTop: 10,
    fontFamily: "Helvetica",
    fontSize: 40,
    fontWeight: "bold",
    color: "#fff"
  },
  flatview: {
    justifyContent: "center",
    paddingTop: 30,
    borderRadius: 2
  },
  name: {
    fontFamily: "Verdana",
    fontSize: 23,
    color: "#fff"
  },
  valid: {
    color: "green",
    fontSize: 23
  },
  btnReturn: {
    marginBottom: 30
  },
  ticketImg: {
    position: "absolute",
    width: "100%",
    height: "30%",
    top: 0,
    left: 0
  },
});
