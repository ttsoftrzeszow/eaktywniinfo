import React, { Component } from "react";
import {
  View,
  StyleSheet, AsyncStorage, ScrollView, Text, TouchableHighlight
} from "react-native";
import { Redirect } from "react-router-native";

class BasketPage extends Component {
  state = {
    itemList: [],
    redirectNFC: false
  };

  componentDidMount = () => {
    AsyncStorage.getItem("basket").then(storageBasket => {
      const basket = JSON.parse(storageBasket);
      this.setState({
        itemList: basket.listItem
      })
    });
  };

  handleRedirectNFCPage = () => {
    this.setState({
      redirectNFC: true
    });
    return true;
  }

  renderRedirect = () => {
    if (this.state.redirectNFC) {
      return <Redirect to={"/nfc/epos"} />;
    }

  };


  render() {
    const basket = this.state.itemList.map((item, index) => (
      <View key={index}>
        <Text style={styles.text}>{parseInt(index) + 1}. {item.assort.name} Ilość: {item.qnt} Cena: {item.assort.listPrice}</Text>
      </View>
    ));

    return (
      <>
        <ScrollView >
          {this.renderRedirect()}
          <View style={styles.container}>
            {basket}

            <View style={[styles.payViewBtn, {marginTop: 20}]}>
              <TouchableHighlight
                style={styles.payBtnActive}
                onPress={() => {
                  this.handleRedirectNFCPage();
                }}
              >
                <Text style={styles.textBtnActive}>Zapłać</Text>
              </TouchableHighlight>
            </View>
          </View>
        </ScrollView>


      </>
    );
  }


}

export default BasketPage;

const styles = StyleSheet.create({
  container: {
    marginTop: 100,
    justifyContent: "center",
    alignItems: "center",
    flexWrap: "wrap",
    width: "100%"
  },
  text: {
    fontWeight: "bold",
    fontSize: 20,
    color: "#fff"
  },
  payBtnActive: {
    backgroundColor: "#4981d5",
    width: "100%",
    height: 80,
    borderRadius: 10,
  },
  textBtnActive: {
    color: "#fff",
    textAlign: "center",
    lineHeight: 80,
    fontSize: 30
  },
  payViewBtn: {
    width: "100%",
    borderWidth: 2,
    borderColor: "#4981d5",
    borderRadius: 10
  },
});
