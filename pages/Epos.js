import React, { Component } from "react";
import {
  View,
  StyleSheet, AsyncStorage, ScrollView
} from "react-native";
import AssortListComponent from "./components/AssortList/AssortListComponent";
import QuantityCalculatorModal from "./modal/QuantityCalculator/QuantityCalculatorModal";
import FooterComponent from "./components/FooterComponent";
import { Redirect } from "react-router-native";

class Epos extends Component {
  state = {
    API_URL: "",
    deviceName: "",
    assortList: [],
    selectedAssort: null,
    quantityModal: false,
    basketLength: 0,
    basketPrice: 0,
    redirectBasket: false
  };

  componentDidMount = () => {
    AsyncStorage.getItem("apiUrl").then(apiUrl => {
      this.setState({ API_URL: apiUrl });
      AsyncStorage.getItem("deviceName").then(deviceName => {
        this.setState({ deviceName });
        this.clearBasket();
        this.getAssortList(null, null, null);
      });
    });
  };

  handleFilterAssort = (group, color, shape) => {
    this.getAssortList(group, color, shape);
  };

  handleAssort = (selectedAssort) => {
    this.setState({
      selectedAssort,
      quantityModal: true
    });
  };

  addAssortToBasket = (qnt) => {
    this.setState({
      quantityModal: false
    });

    AsyncStorage.getItem("basket").then(storageBasket => {
      const basket = JSON.parse(storageBasket);
      this.addToBasket(basket, this.state.selectedAssort, qnt);
    });
  };

  handleRedirectAssortPage = () => {
    this.setState({
      redirectBasket: true
    });
    return true;
  }

  renderRedirect = () => {
    if (this.state.redirectBasket) {
      return <Redirect to={"/basket"} />;
    }

  };

  render() {
    return (
      <>
        <ScrollView >
          {this.renderRedirect()}
          <View style={styles.container}>
            <AssortListComponent assortList={this.state.assortList}
                                 apiUrl={this.state.API_URL}
                                 onFilterAssort={(group, color, shape) => this.handleFilterAssort(group, color, shape)}
                                 selectAssort={(assort) => this.handleAssort(assort)}
            />
          </View>

          <QuantityCalculatorModal visible={this.state.quantityModal} addAssortToBasket={(qnt) => this.addAssortToBasket(qnt)}/>
        </ScrollView>
        <FooterComponent basketPrice={this.state.basketPrice} basketLength={this.state.basketLength} onNext={() => this.handleRedirectAssortPage()}/>
      </>
    );
  }

  getAssortList = (group, color, shape) => {
    let url = `${this.state.API_URL}/crud/assort/pos?ip=${this.state.deviceName}`;
    if(group) {
      url += `&groupIds=${this.getIdsNumber(group)}`
    }
    if(color) {
      url += `&colorIds=${this.getIdsNumber(color)}`
    }
    if(shape) {
      url += `&shapeIds=${this.getIdsNumber(shape)}`
    }
    fetch(url)
      .then(response => {
        if (response.ok) {
          return response;
        }
        throw Error("Nie udało się");
      })
      .then(response => response.json())
      .then(data => {
        if (!data.isError) {
          console.log(data);
          this.setState({
            assortList: data.result
          });
        } else {
          if (data.errorTable.length > 0) {
            alert(data.errorTable[0]);
          } else {
            alert("Nieznany błąd");
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  getIdsNumber = (value): string => {
    if (value) {
      return `${value.objectDb},${value.objectId}`;
    } else {
      return "";
    }
  }

  addToBasket = (basket, assort, qnt) => {
    let item = {
      assort: assort,
      qnt: qnt,
      itemPrice: parseFloat(qnt) * parseFloat(assort.listPrice)
    }
    basket.listItem.push(item);
    basket.basketPrice = parseFloat(basket.basketPrice) + parseFloat(item.itemPrice);
    basket.basketLength = parseFloat(basket.basketLength) + parseFloat(item.qnt);
    AsyncStorage.setItem("basket", JSON.stringify(basket));
    console.log(basket);

    this.setState({
      basketLength: basket.basketLength,
      basketPrice: basket.basketPrice
    })
  }

  clearBasket = () => {
    const basket = {
      listItem: [],
      basketPrice: 0,
      basketLength: 0
    };
  AsyncStorage.setItem("basket", JSON.stringify(basket));
  }

}

export default Epos;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 100,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    flexWrap: "wrap",
  }
});
