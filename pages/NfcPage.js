import React, { Component } from "react";
import {
  View,
  Keyboard,
  StyleSheet,
  TextInput,
  Button,
  Platform,
  Image, AsyncStorage
} from "react-native";
import NfcManager from "react-native-nfc-manager";
import { Redirect } from "react-router-native";

class NfcPage extends Component {
  state = {
    trhId: "",
    page: this.props.match.params.page,

    supported: true,
    enabled: false,
    tag: {},

    saveError: "",
    saveSuccess: "",
    errorType: "",
    redirectFinalize: false
  };

  sendTrhId = () => {
    this.onSubmitTrhId(this.state.trhId);
  };

  onSubmitTrhId = trhId => {
    console.log("Page: " + this.state.page);
    if (this.state.page === "rent") {
      this.props.rentAssort(trhId, this.props.assort);
    }

    if (this.state.page === "ticketInfo") {
      let trhIdString = trhId.replace("/", "-");
      trhIdString = trhIdString.replace("/", "-");
      this.props.history.push({
        pathname: `/${this.state.page}/${trhIdString}`
      });
    } else if (this.state.page === "epos") {
      this.saveBill(trhId);
    } else {
      this.props.history.push({
        pathname: `/${this.state.page}/${trhId}`
      });
    }
  };

  clearTrhId = () => {
    this.setState({
      trhId: ""
    });
  };

  componentDidMount() {
    NfcManager.isSupported().then(supported => {
      this.setState({ supported });
      if (supported) {
        this._startNfc();
      }
    });
  }

  handleRedirectFinalizePage = () => {
    this.setState({
      redirectFinalize: true
    });
    return true;
  }

  renderRedirect = () => {
    if (this.state.redirectFinalize) {
      return <Redirect to={{pathname: "/finalize", state: {saveError: this.state.saveError, saveSuccess: this.state.saveSuccess, errorType: this.state.errorType, trhId: this.state.trhId}}} />;
    }

  };

  render() {
    const text_1 = `${this.state.page === "accountInfo" ? "kartę" : "pasek"}`;
    const text_2 = `${this.state.page === "accountInfo" ? "karty" : "paska"}`;
    let btnColor = "#0AB6CE";
    let gif = (
      <Image
        style={{
          width: 300,
          height: 300,
          alignItems: "center",
          justifyContent: "center"
        }}
        source={require("../images/reader-blue.gif")}
      />
    );
    if (this.state.page === "wristlet") {
      btnColor = "#ffa736";
      gif = (
        <Image
          style={{
            width: 300,
            height: 300,
            alignItems: "center",
            justifyContent: "center"
          }}
          source={require("../images/reader-orange.gif")}
        />
      );
    } else if (this.state.page === "accountInfo") {
      btnColor = "#2cc884";
      gif = (
        <Image
          style={{
            width: 300,
            height: 300,
            alignItems: "center",
            justifyContent: "center"
          }}
          source={require("../images/reader-green.gif")}
        />
      );
    } else if (this.state.page === "rent") {
      btnColor = "#4981d5";
    }
    return (
      <>
        {this.renderRedirect()}
        <View style={styles.containerNfc}>
          {/* <Text style={styles.nfcText}>
          Przyłóż {text_1} do czytnika NFC lub wpisz numer {text_2} w polu
          poniżej
        </Text> */}
          {gif}
          <TextInput
            style={[styles.formInput, { borderBottomColor: btnColor }]}
            value={this.state.trhId}
            // onChangeText={trhId => this.setState({ trhId })}
            onChangeText={trhId => this.onSubmitTrhId(trhId)}
            placeholder={`Podaj numer ${text_2}`}
            placeholderTextColor="#ccc"
            // onFocus={this.clearTrhId}
            onFocus={Keyboard.dismiss}
            autoFocus={true}
          />
          <View style={{ width: "60%" }}>
            <Button
              title="Wyślij"
              color={btnColor}
              disabled={!this.state.trhId}
              onPress={this.sendTrhId}
            />
          </View>
        </View>
      </>
    );
  }

  _startNfc() {
    NfcManager.start({
      onSessionClosedIOS: () => {
        console.log("ios session closed");
      }
    })
      .then(result => {
        console.log("start OK", result);
      })
      .catch(error => {
        console.warn("start fail", error);
        this.setState({ supported: false });
      });

    if (Platform.OS === "android") {
      NfcManager.getLaunchTagEvent()
        .then(tag => {
          console.log("launch tag", tag);
          if (tag) {
            this.setState({ tag });
          }
          this._startDetection();
        })
        .catch(err => {
          console.log(err);
        });
      NfcManager.isEnabled()
        .then(enabled => {
          this.setState({ enabled });
        })
        .catch(err => {
          console.log(err);
        });
      NfcManager.onStateChanged(event => {
        if (event.state === "on") {
          this.setState({ enabled: true });
        } else if (event.state === "off") {
          this.setState({ enabled: false });
        } else if (event.state === "turning_on") {
          // do whatever you want
        } else if (event.state === "turning_off") {
          // do whatever you want
        }
      });
    }
  }

  _startDetection = () => {
    NfcManager.registerTagEvent(this._onTagDiscovered)
      .then(result => {
        console.log("registerTagEvent OK", result);
      })
      .catch(error => {
        console.warn("registerTagEvent fail", error);
      });
  };

  _goToNfcSetting = () => {
    if (Platform.OS === "android") {
      NfcManager.goToNfcSetting()
        .then(result => {
          console.log("goToNfcSetting OK", result);
        })
        .catch(error => {
          console.warn("goToNfcSetting fail", error);
        });
    }
  };

  _onTagDiscovered = tag => {
    console.log("Tag Discovered NFC", tag);
    this.setState({
      tag,
      trhId: `M1${tag.id}`
    });
    this._stopDetection();
  };

  _stopDetection = () => {
    NfcManager.unregisterTagEvent()
      .then(result => {
        console.log("unregisterTagEvent OK", result);
        this.sendTrhId();
      })
      .catch(error => {
        console.warn("unregisterTagEvent fail", error);
      });
  };

  saveBill = (trhId) => {

    AsyncStorage.getItem("apiUrl").then(apiUrl => {
      AsyncStorage.getItem("basket").then(storageBasket => {
        const basket = JSON.parse(storageBasket);
        const lines = [];
        basket.listItem.forEach(function (item) {
          const TicketBillLinePOJO = {
            assort: item.assort,
            quantity: 1,
            ticketAccessList: item.ticketAccessList
          }

          lines.push(TicketBillLinePOJO);
        })

        let TicketBillHolderPOJO = {
          accountPOJO: null,
          purchaser: null,
          payType: 'M',
          documentType: "RA",
          ipAddress: "",
          lines: lines,
          createInvoice: false,
          recipient: null,
          trhId: trhId
        }

        const url = `${apiUrl}/admin_creat_new_bill`;
        fetch(url, {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(TicketBillHolderPOJO)
        })
          .then(response => {
            if (response.ok) {
              return response;
            }
            throw Error("Nie udało się");
          })
          .then(response => response.json())
          .then(data => {
            console.log(data);
            if (!data.isError) {
              this.setState({saveSuccess: "Transakcja przebiegła pomyślnie"});
              this.handleRedirectFinalizePage();
            } else {
              if (data.errorTable.length  > 0) {
                this.setState({
                  saveError: data.errorTable[0],
                  errorType: data.messageType
                });
              } else {
                this.setState({
                  saveError: "Nieznany błąd",
                  errorType: data.messageType
                });
              }
              this.handleRedirectFinalizePage();
            }
          })
          .catch(err => {
            console.log(err);
          });

      });
    });


  }
}

export default NfcPage;

const styles = StyleSheet.create({
  containerNfc: {
    padding: 20,
    alignItems: "center"
  },
  nfcText: {
    color: "red",
    fontSize: 20
  },
  formInput: {
    borderBottomWidth: 1,
    margin: 20,
    padding: 5,
    width: "70%",
    color: "#ccc",
    textAlign: "center"
  }
});
