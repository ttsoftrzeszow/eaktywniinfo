import React, { Component } from "react";
import {
  Modal,
  Text,
  TouchableHighlight,
  View,
  StyleSheet,
  TextInput
} from "react-native";

class SettingsModal extends Component {
  state = {
    modalVisible: false,
    deviceName: "",
    prefix: "https://panel.eaktywni.pl:442/tt-admin"
  };

  setModalVisible(visible) {
    this.props.apiSettings(
      this.state.deviceName,
      this.state.prefix
    );
    this.setState({ modalVisible: visible });
  }

  render() {
    return (
      <View style={{ marginTop: 22 }}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
        >
          <View style={styles.containerModal}>
            <View>
              <Text style={styles.modalHeaderText}>Ustawienia</Text>

              {/*<TextInput*/}
                {/*style={styles.formInput}*/}
                {/*value={this.state.prefix}*/}
                {/*onChangeText={prefix => this.setState({ prefix })}*/}
                {/*placeholder="Podaj adres API"*/}
                {/*placeholderTextColor="#ccc"*/}
              {/*/>*/}

              <TextInput
                style={styles.formInput}
                value={this.state.deviceName}
                onChangeText={deviceName => this.setState({ deviceName })}
                placeholder="podaj nazwę urządzenia"
                placeholderTextColor="#ccc"
              />

              <TouchableHighlight
                style={{ width: "100%" }}
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}
              >
                <Text style={styles.saveBtn}>Zapisz</Text>
              </TouchableHighlight>
            </View>
          </View>
        </Modal>

        <TouchableHighlight
          onPress={() => {
            this.setModalVisible(true);
          }}
        >
          <Text style={{ marginLeft: 10 }}>Ustawienia</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

export default SettingsModal;

const styles = StyleSheet.create({
  containerModal: {
    flex: 1,
    marginTop: 22,
    justifyContent: "center",
    alignItems: "center"
  },
  modalHeaderText: {
    justifyContent: "center",
    fontSize: 20
  },
  formInput: {
    padding: 10,
    width: "90%",
    borderBottomWidth: 1,
    borderColor: "#ccc",
    justifyContent: "center",
    color: "#ccc",
    textAlign: "center",
    fontSize: 20,
    fontStyle: "italic"
  },
  saveBtn: {
    textAlign: "center",
    lineHeight: 40,
    marginTop: 20,
    backgroundColor: "#2cc8c5",
    width: 150,
    height: 40,
    borderRadius: 10,
    color: "white"
  }
});
