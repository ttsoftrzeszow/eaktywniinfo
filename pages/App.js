/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  ImageBackground,
  Text,
  AsyncStorage
} from "react-native";
import LoginPage from "./LoginPage";
import Secured from "./Secured";

export default class App extends Component {
  state = {
    isLoggedIn: false,
    operator: ""
  };

  handleSetOperatorLogin = operator => {
    //mk03072020 rezygnujemy z ustawien, zamiast tego w divaceName ustawiam token zalogowanego operatora
    AsyncStorage.setItem("deviceName", operator.token);
    this.setState({
      isLoggedIn: true,
      operator
    });
  };

  handleSetAPI = (deviceName, prefix) => {
    let api = prefix;
    // let api = "https://panel.eaktywni.pl:442/tt-admin";
    // deviceName = "192.168.15.100";
    AsyncStorage.setItem("apiUrl", api);
    AsyncStorage.setItem("deviceName", deviceName);
  };

  render() {
    if (this.state.isLoggedIn) {
      return (
        <View style={styles.container}>
          <Secured
            {...this.props}
            onLogoutPress={() => this.setState({ isLoggedIn: false })}
            operatorLogin={this.state.operator}
          />
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <ImageBackground
            source={require("../images/bck-main.jpg")}
            style={{
              width: "100%",
              height: "100%",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Image
              style={styles.logo}
              source={require("../images/logo-eaktywni.png")}
              resizeMode="center"
            />
            <Text style={styles.textLogin}>Logowanie</Text>
            <View style={styles.loginContainer}>
              <LoginPage
                onLoginPress={this.handleSetOperatorLogin}
                setAPI={this.handleSetAPI}
              />
            </View>
            <Text style={styles.textwww}>www.eaktywni.pl</Text>
          </ImageBackground>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#191A21"
  },
  logo: {
    position: "absolute",
    top: "20%",
    width: "30%",
    height: "10%"
  },
  loginContainer: {
    position: "absolute",
    top: "40%",
    left: 0,
    width: "90%",
    height: 160
  },
  textLogin: {
    position: "absolute",
    top: "30%",
    fontSize: 25,
    marginTop: 20,
    fontWeight: "bold"
  },
  textwww: {
    position: "absolute",
    alignItems: "center",
    fontSize: 20,
    bottom: "5%"
  }
});
