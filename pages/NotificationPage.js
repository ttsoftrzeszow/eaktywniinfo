import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  FlatList,
  AsyncStorage
} from "react-native";

class NotificationPage extends Component {
  state = {
    alerts: [],
    API_URL: "",
    computerName: ""
  };

  componentDidMount() {
    let api = "";
    AsyncStorage.getItem("apiUrl").then(apiUrl => {
      this.setState({ API_URL: apiUrl });
      api = apiUrl;
    });
    AsyncStorage.getItem("deviceName").then(deviceName => {
      this.setState({ computerName: deviceName });
      this.getCopmuterAlerts(api, deviceName);
    });
  }

  getCopmuterAlerts = (api, device) => {
    const url = `${api}/crud/computerAlert/getForIp?ip=${device}`;
    fetch(url)
      .then(response => {
        if (response.ok) {
          return response;
        }
        throw Error("Nie udało się");
      })
      .then(response => response.json())
      .then(data => {
        if (!data.isError) {
          this.setState({
            alerts: data.result
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    return (
      <>
        <View style={styles.containerPass}>
          <Text style={styles.h2text}>Powiadomienia</Text>
          <FlatList
            data={this.state.alerts}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => (
              <View style={styles.flatview}>
                <Text style={styles.name}>{item.createOperatorName}</Text>
                <Text>{item.activateTime}</Text>
                <Text style={styles.valid}>{item.computerAlert}</Text>
              </View>
            )}
            keyExtractor={item => `${item.objectId}o`}
          />
        </View>
      </>
    );
  }
}

export default NotificationPage;

const styles = StyleSheet.create({
  containerPass: {
    flex: 1,
    marginTop: "20%",
    justifyContent: "center",
    alignItems: "center"
  },
  h2text: {
    marginTop: 10,
    fontFamily: "Helvetica",
    fontSize: 36,
    fontWeight: "bold",
    color: "#fff"
  },
  h2textError: {
    marginTop: 10,
    fontFamily: "Helvetica",
    fontSize: 26,
    fontWeight: "bold",
    color: "red"
  },
  flatview: {
    justifyContent: "center",
    padding: 30,
    borderBottomWidth: 1,
    borderColor: "#ccc"
  },
  name: {
    fontFamily: "Verdana",
    fontSize: 18,
    color: "#fff"
  },
  valid: {
    color: "red"
  }
});
