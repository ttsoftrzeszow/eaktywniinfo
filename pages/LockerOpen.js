import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Button,
  Alert,
  TextInput,
  AsyncStorage,
  Text
} from "react-native";
import axios from "axios";

class LockerOpen extends Component {
  state = {
    API_URL: "",
    lockerLogo: "",
    locker: ""
  };

  componentDidMount = () => {
    AsyncStorage.getItem("apiUrl").then(apiUrl => {
      this.setState({ API_URL: apiUrl });
    });
  };

  handleInfoLocker = () => {
    const url = `${this.state.API_URL}/crud/locker/info?logo=${
      this.state.lockerLogo
    }`;
    this.infoLocker(url);
  };

  handleOpenLocker = () => {
    const params = {
      lockerIds: `${this.state.locker.objectDb},${this.state.locker.objectId}`
    };
    const url = `${this.state.API_URL}/crud/locker/open`;
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(params)
    })
      .then(response => {
        if (response.ok) {
          console.log(response.ok);
          return response;
        }
        throw Error("Nie udało się");
      })
      .then(response => response.json())
      .then(data => {
        if (!data.isError) {
          Alert.alert(
            "Sukces!",
            `Otwarto szafkę: ${this.state.locker.logo}`,
            [{ text: "OK", onPress: () => console.log("OK Pressed") }],
            { cancelable: false }
          );
        } else {
          Alert.alert(
            "Błąd!",
            `${data.errorTable[0]}`,
            [{ text: "OK", onPress: () => console.log("OK Pressed") }],
            { cancelable: false }
          );
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleBlockLocker = () => {
    const params = {
      objectDb: `${this.state.locker.objectDb}`,
      objectId: `${this.state.locker.objectId}`,
      status: "B"
    };
    const url = `${this.state.API_URL}/crud/locker/status`;
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(params)
    })
      .then(response => {
        if (response.ok) {
          console.log(response.ok);
          return response;
        }
        throw Error("Nie udało się");
      })
      .then(response => response.json())
      .then(data => {
        if (!data.isError) {
          const url = `${this.state.API_URL}/crud/locker/info?logo=${
            this.state.locker.logo
          }`;
          this.infoLocker(url);
        } else {
          Alert.alert(
            "Błąd!",
            `${data.errorTable[0]}`[
              { text: "OK", onPress: () => console.log("OK Pressed") }
            ],
            { cancelable: false }
          );
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleUnBlockLocker = () => {
    const params = {
      objectDb: `${this.state.locker.objectDb}`,
      objectId: `${this.state.locker.objectId}`,
      status: "A"
    };
    const url = `${this.state.API_URL}/crud/locker/status`;
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(params)
    })
      .then(response => {
        if (response.ok) {
          console.log(response.ok);
          return response;
        }
        throw Error("Nie udało się");
      })
      .then(response => response.json())
      .then(data => {
        if (!data.isError) {
          const url = `${this.state.API_URL}/crud/locker/info?logo=${
            this.state.locker.logo
          }`;
          this.infoLocker(url);
        } else {
          Alert.alert(
            "Błąd!",
            `${data.errorTable[0]}`[
              { text: "OK", onPress: () => console.log("OK Pressed") }
            ],
            { cancelable: false }
          );
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  clearLockerLogo = () => {
    this.setState({
      lockerLogo: ""
    });
  };

  infoLocker = url => {
    axios
      .get(url)
      .then(response => {
        if (response.data.isError) {
          Alert.alert(
            "Błąd!",
            `${response.data.errorTable[0]}`,
            [{ text: "OK", onPress: () => console.log("OK Pressed") }],
            { cancelable: false }
          );
        } else {
          console.log(response.data.result);
          this.setState({
            lockerLogo: "",
            locker: response.data.result
          });
        }
      })
      .catch(error => {
        Alert.alert(
          "Błąd!",
          `${error}`,
          [{ text: "OK", onPress: () => console.log("OK Pressed") }],
          { cancelable: false }
        );
      });
  };

  render() {
    let lockerStatus = "-";
    let statusColor = "#ccc";
    if (this.state.locker.status === "A") {
      lockerStatus = "Wolna";
      statusColor = "#2cc884";
    } else if (this.state.locker.status === "B") {
      lockerStatus = "Uszkodzona";
      statusColor = "#ccc";
    } else if (this.state.locker.status === "U") {
      lockerStatus = "Zajęta";
      statusColor = "#f42363";
    } else {
      lockerStatus = "-";
      statusColor = "#ccc";
    }
    return (
      <View style={styles.containerLockerOpen}>
        <TextInput
          style={styles.formInput}
          value={this.state.lockerLogo}
          onChangeText={lockerLogo => this.setState({ lockerLogo })}
          placeholder="Podaj logo szafki"
          placeholderTextColor="#ccc"
          onFocus={this.clearLockerLogo}
        />
        <View style={{ width: "100%" }}>
          <Button
            title="Wyślij"
            color="#4981d5"
            disabled={!this.state.lockerLogo}
            onPress={this.handleInfoLocker}
          />
        </View>
        {!!this.state.locker && (
          <View style={styles.infoLocker}>
            <Text style={styles.textLogo}>
              Logo szafki:{" "}
              <Text
                style={{ color: "#4981d5", fontWeight: "bold", fontSize: 30 }}
              >
                {this.state.locker.logo}
              </Text>
            </Text>

            <Text style={styles.textLogo}>
              Status:{" "}
              <Text
                style={{ color: statusColor, fontWeight: "bold", fontSize: 25 }}
              >
                {lockerStatus}
              </Text>
            </Text>

            <View style={{ width: "100%", marginTop: 20 }}>
              <Button
                title="Otwórz szafkę"
                color="#4981d5"
                style={{ fontSize: 15 }}
                disabled={!this.state.locker}
                onPress={this.handleOpenLocker}
              />
            </View>

            {this.state.locker.status !== "B" && (
              <View style={{ width: "100%", marginTop: 20 }}>
                <Button
                  title="Zablokuj szafkę"
                  color="#4981d5"
                  style={{ fontSize: 15 }}
                  disabled={!this.state.locker}
                  onPress={this.handleBlockLocker}
                />
              </View>
            )}

            {this.state.locker.status === "B" && (
              <View style={{ width: "100%", marginTop: 20 }}>
                <Button
                  title="Odblokuj szafkę"
                  color="#4981d5"
                  style={{ fontSize: 15 }}
                  disabled={!this.state.locker}
                  onPress={this.handleUnBlockLocker}
                />
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}

export default LockerOpen;

const styles = StyleSheet.create({
  containerLockerOpen: {
    padding: 20,
    alignItems: "center"
  },
  formInput: {
    borderBottomWidth: 1,
    borderBottomColor: "#4981d5",
    margin: 20,
    padding: 5,
    width: "100%",
    color: "#ccc",
    textAlign: "center"
  },
  infoLocker: {
    padding: 30,
    width: "100%",
    alignItems: "center"
  },
  textLogo: {
    color: "#fff",
    fontSize: 25
  }
});
