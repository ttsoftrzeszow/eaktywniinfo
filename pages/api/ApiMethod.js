export const postRestBridge = async (url, params = {}) => {
  let aEntity = null;
  fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(params)
  })
    .then(response => {
      if (response.ok) {
        console.log(response.ok);
        return response;
      }
      throw Error("Nie udało się");
    })
    .then(response => response.json())
    .then(data => {
      aEntity = data;
    })
    .catch(err => {
      console.log(err);
    });

  return aEntity;
};

export const getRestBridge = async (url) => {
  let aEntity = null;
  fetch(url)
    .then(response => {
      if (response.ok) {
        return response;
      }
      throw Error("Nie udało się");
    })
    .then(response => response.json())
    .then(data => {
      aEntity = data;
    })
    .catch(err => {
      console.log(err);
    });

  return await aEntity;
};

export const getHeaders = (token) => {
  return {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "X-Auth-User-Token": `${token}`
    }
  }
};

export const postHeaders = (token, params = {}) => {
  return {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-Auth-User-Token": `${token}`
    },
    body: JSON.stringify(params)
  }
};

export const getIds = (item) => {
  const ids = item ? `${item.objectDb},${item.objectId}` : "";
  return ids !== "0,0" ? ids : "";
};

