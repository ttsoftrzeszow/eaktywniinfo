import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
  ScrollView,
  AsyncStorage
} from "react-native";
import NfcPage from "./NfcPage";

class RentPage extends Component {
  state = {
    assortRentList: [],
    selectAssort: "",
    API_URL: ""
  };

  componentDidMount = () => {
    AsyncStorage.getItem("apiUrl").then(apiUrl => {
      this.setState({ API_URL: apiUrl });
      this.getAssortRent(apiUrl);
    });
  };

  getAssortRent = apiUrl => {
    const url = `${apiUrl}/crud/assort/rent`;
    fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.ok) {
          console.log(response.ok);
          return response;
        }
        throw Error("Nie udało się");
      })
      .then(response => response.json())
      .then(data => {
        if (!data.isError) {
          this.setState({
            assortRentList: data.result
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleSelectAssort = assort => {
    this.setState({
      selectAssort: assort
    });
  };

  rentAssort = (trhId, assort) => {
    const addAssortToWristletSimplePOJO = {
      trhId: trhId,
      assort: assort
    };
    const url = `${this.state.API_URL}/basket/entry/addRentAssort/trhId`;
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(addAssortToWristletSimplePOJO)
    })
      .then(response => {
        if (response.ok) {
          console.log(response.ok);
          return response;
        }
        throw Error("Nie udało się");
      })
      .then(response => response.json())
      .then(data => {
        this.setState({
          selectAssort: ""
        });
        if (!data.isError) {
          Alert.alert(
            "Sukces!",
            "Poprawnie przypisano wypożyczenie",
            [{ text: "OK", onPress: () => console.log("OK Pressed") }],
            { cancelable: false }
          );
        } else {
          Alert.alert(
            "Błąd",
            `${data.errorTable[0]}`,
            [{ text: "OK", onPress: () => console.log("OK Pressed") }],
            { cancelable: false }
          );
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    if (this.state.selectAssort) {
      this.props.match.params.page = "rent";
      return (
        <>
          <NfcPage
            {...this.props}
            rentAssort={this.rentAssort}
            assort={this.state.selectAssort}
          />
        </>
      );
    } else {
      const assortRent = this.state.assortRentList.map(item => (
        <View key={item.name} style={styles.menuBtn}>
          <TouchableOpacity
            style={styles.assortRentBtn}
            onPress={() => this.handleSelectAssort(item)}
          >
            <Text style={{ color: "#fff", textAlign: "center", padding: 5 }}>
              {item.name}
            </Text>
            <Text style={{ color: "#fff", textAlign: "center" }}>
              Cena: {item.price}
            </Text>
          </TouchableOpacity>
        </View>
      ));
      return (
        <ScrollView>
          <View style={styles.containerRentAssort}>
            <Text style={styles.textRent}>Wypożyczalnia</Text>
            {assortRent}
          </View>
        </ScrollView>
      );
    }
  }
}

export default RentPage;

const styles = StyleSheet.create({
  containerRentAssort: {
    flex: 1,
    marginTop: 90,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    flexWrap: "wrap",
  },
  textRent: {
    width: "100%",
    textAlign: "center",
    fontFamily: "Helvetica",
    fontSize: 20,
    fontWeight: "bold",
    color: "#fff"
  },
  assortRentBtn: {
    backgroundColor: "#4981d5",
    width: "100%",
    height: 80,
    alignItems: "center",
    fontSize: 20,
    textAlign: "center",
    borderRadius: 5
  },
  textBtn: {
    position: "absolute",
    bottom: 0,
    color: "#fff"
  },
  menuBtn: {
    width: "40%",
    marginTop: 20
  }
});
