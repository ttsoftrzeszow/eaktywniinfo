import React, { Component } from "react";
import {getHeaders, getIds, postHeaders} from "./api/ApiMethod";
import {
  View,
  Text,
  ScrollView,
  FlatList,
  AsyncStorage,
  StyleSheet,
  Button,
} from "react-native";

class ZoneInfo extends Component {
  state = {
    zoneList: [],
    API_URL: "",
    deviceName: "",
  };

  componentDidMount = () => {
    AsyncStorage.getItem("apiUrl").then((apiUrl) => {
      this.setState({ API_URL: apiUrl });
      AsyncStorage.getItem("deviceName").then((deviceName) => {
        this.setState({ deviceName });
        this.getZoneList(apiUrl, deviceName);
      });
    });
  };

  getZoneList = (apiUrl, deviceName) => {
    const url = `${apiUrl}/EAktywni/entry/zones`;
    fetch(url, getHeaders(deviceName))
      .then((response) => {
        if (response.ok) {
          console.log(response.ok);
          return response;
        }
        throw Error("Nie udało się");
      })
      .then((response) => response.json())
      .then((data) => {
        if (!data.isError) {
          this.setState({
            zoneList: data.result,
          });
        } else {
          this.errorAlert(data.errorTable);
        }
      })
      .catch((err) => {
        this.errorAlert([err]);
      });
  };

  handleSelectZone = (zoneIds) => {
    const url = `${this.state.API_URL}/EAktywni/entry/zones`;
    fetch(url, postHeaders(this.state.deviceName, { zoneIds }))
      .then((response) => {
        if (response.ok) {
          console.log(response.ok);
          return response;
        }
        throw Error("Nie udało się");
      })
      .then((response) => response.json())
      .then((data) => {
        if (!data.isError) {
            this.getZoneList(this.state.API_URL, this.state.deviceName);
        } else {
          this.errorAlert(data.errorTable);
        }
      })
      .catch((err) => {
        this.errorAlert([err]);
      });
  };

  render() {
    return (
      <ScrollView>
        <View style={styles.containerOccupancy}>
          <View>
            <FlatList
              data={this.state.zoneList}
              renderItem={({ item }) => (
                <>
                  <View style={styles.zone}>
                    <Text
                      style={[
                        styles.zoneText,
                        {
                          width: "70%",
                          color: item.checked ? "#00e33d" : "#fff",
                        },
                      ]}
                    >
                      {item.name}{" "}
                    </Text>
                    <Button
                      style={{ width: "20%", height: 80 }}
                      title="Wybierz"
                      color="#4981d5"
                      onPress={() => this.handleSelectZone(getIds(item))}
                    />
                  </View>
                  <View style={styles.hr} />
                </>
              )}
              keyExtractor={(item) => `${item.objectId}o`}
            />
          </View>
        </View>
        <Text>{this.state.message}</Text>
      </ScrollView>
    );
  }

  errorAlert = (error) => {
    if (error.length === 0) {
      error = ["Wystapił nieznany błąd"];
    }
    Alert.alert(
      "Błąd!",
      `${error[0]}`,
      [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      { cancelable: false }
    );
  };
}

export default ZoneInfo;

const styles = StyleSheet.create({
  containerOccupancy: {
    flex: 1,
    marginTop: 100,
  },
  zone: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10,
  },
  hr: {
    marginLeft: "20%",
    borderBottomWidth: 1,
    borderBottomColor: "#f42363",
    width: "50%",
  },
  zoneText: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold",
  },
});
