import React, { Component } from "react";
import {
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
  Text,
  ActivityIndicator,
  Platform,
  Image,
  AsyncStorage,
  Alert
} from "react-native";
import NfcManager from "react-native-nfc-manager";

class LoginPage extends Component {
  state = {
    login: "",
    password: "",
    isLoggingIn: false,
    message: "",

    supported: true,
    enabled: false,
    tag: {},
    API_URL: "https://panel.eaktywni.pl:442/tt-admin"
    // API_URL: "http://192.168.147.30:8085/tt-rest"
  };

  componentDidMount() {
    const basket = {
      listItem: [],
      basketPrice: 0,
      basketLength: 0
    };
    AsyncStorage.setItem("basket", JSON.stringify(basket));
    AsyncStorage.setItem("apiUrl", this.state.API_URL);
    NfcManager.isSupported().then(supported => {
      this.setState({ supported });
      if (supported) {
        this._startNfc();
      }
    });
  }

  userLogin = () => {
    this.setState({ isLoggingIn: true, message: "" });
    const urlLogin = `${this.state.API_URL}/EAktywni/operator/login`;
    this.logInPassword(urlLogin);
  };

  userLoginByTrhId = () => {
    const trhId = `M1${this.state.tag.id}`;
    this.setState({ isLoggingIn: true, message: "" });
    const urlLogin = `${this.state.API_URL}/crud/operator/trhid?trhId=${trhId}`;

    this.logInTrhId(urlLogin);
  };

  logInPassword = urlLogin => {
    fetch(urlLogin, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({login: this.state.login, password: this.state.password})
    })
        .then(response => {
          if (response.ok) {
            return response;
          } else {
            this.setState({
              isLoggingIn: false
            });
            this.openNFCConnection();
            Alert.alert("Wystąpił wyjątek!", "Brak połącznia z siecią");

          }
        })
        .then(response => response.json())
        .then(data => {
          if (data.isError) {
            this.setState({
              isLoggingIn: false
            });
            this.openNFCConnection();
            Alert.alert("Wystąpił wyjątek!", `${data.errorTable[0]}`);
          } else {
            if (data.result) {
              this.setState({ isLoggingIn: false });
              this.props.onLoginPress(data.result);
            } else {
              this.setState({
                isLoggingIn: false
              });
              this.openNFCConnection();
              Alert.alert("Wystąpił wyjątek!", "Błędny login lub hasło");

            }
          }
        })
        .catch(err => {
          this.setState({
            isLoggingIn: false
          });
          this.openNFCConnection();
          Alert.alert("Wystąpił wyjątek!", `${err}`);

        });
  };

  logInTrhId = urlLogin => {
    fetch(urlLogin)
      .then(response => {
        if (response.ok) {
          return response;
        } else {
          this.setState({
            isLoggingIn: false
          });
          this.openNFCConnection();
          Alert.alert("Wystąpił wyjątek!", "Brak połącznia z siecią");

        }
      })
      .then(response => response.json())
      .then(data => {
        if (data.isError) {
          this.setState({
            isLoggingIn: false
          });
          this.openNFCConnection();
          Alert.alert("Wystąpił wyjątek!", `${data.errorTable[0]}`);
        } else {
          if (data.result) {
            this.setState({ isLoggingIn: false });
            this.props.onLoginPress(data.result);
          } else {
            this.setState({
              isLoggingIn: false
            });
            this.openNFCConnection();
            Alert.alert("Wystąpił wyjątek!", "Błędny login lub hasło");

          }
        }
      })
      .catch(err => {
        this.setState({
          isLoggingIn: false
        });
        this.openNFCConnection();
        Alert.alert("Wystąpił wyjątek!", `${err}`);

      });
  };

  clearLogin = () => {
    this.setState({
      login: "",
      message: ""
    });
  };

  clearPassword = () => {
    this.setState({
      password: "",
      message: ""
    });
  };

  render() {
    return (
      <>
        <View style={styles.loginView}>
          <TextInput
            style={styles.formInput}
            value={this.state.login}
            onChangeText={login => this.setState({ login })}
            placeholder="login"
            placeholderTextColor="#ccc"
            onFocus={this.clearLogin}
          />
          <TextInput
            style={[styles.formInput, styles.inputPasword]}
            value={this.state.password}
            onChangeText={password => this.setState({ password })}
            secureTextEntry={true}
            placeholder="hasło"
            placeholderTextColor="#ccc"
            onFocus={this.clearPassword}
          />
          {!!this.state.message && (
            <Text style={styles.warningInfo}>{this.state.message}</Text>
          )}

          {/* <View style={styles.loginBtn}> */}
          <TouchableOpacity onPress={this.userLogin} style={styles.loginBtn}>
            <Image
              style={styles.loginBtnImage}
              source={require("../images/arrow-right.png")}
            />
          </TouchableOpacity>
          {/* </View> */}
        </View>
        {this.state.isLoggingIn && <ActivityIndicator />}
        <View style={{ margin: 7 }} />
        {/*<SettingsModal apiSettings={this.props.setAPI} />*/}
      </>
    );
  }

  openNFCConnection = () => {
    NfcManager.isSupported().then(supported => {
      this.setState({ supported });
      if (supported) {
        this._startNfc();
      }
    });
  }

  _startNfc() {
    NfcManager.start({
      onSessionClosedIOS: () => {
        console.log("ios session closed");
      }
    })
      .then(result => {
        console.log("start OK", result);
      })
      .catch(error => {
        console.warn("start fail", error);
        this.setState({ supported: false });
      });

    if (Platform.OS === "android") {
      NfcManager.getLaunchTagEvent()
        .then(tag => {
          console.log("launch tag", tag);
          if (tag) {
            this.setState({ tag });
          }
          this._startDetection();
        })
        .catch(err => {
          console.log(err);
        });
      NfcManager.isEnabled()
        .then(enabled => {
          this.setState({ enabled });
        })
        .catch(err => {
          console.log(err);
        });
      NfcManager.onStateChanged(event => {
        if (event.state === "on") {
          this.setState({ enabled: true });
        } else if (event.state === "off") {
          this.setState({ enabled: false });
        } else if (event.state === "turning_on") {
          // do whatever you want
        } else if (event.state === "turning_off") {
          // do whatever you want
        }
      });
    }
  }

  _startDetection = () => {
    NfcManager.registerTagEvent(this._onTagDiscovered)
      .then(result => {
        console.log("registerTagEvent OK", result);
      })
      .catch(error => {
        console.warn("registerTagEvent fail", error);
      });
  };

  _goToNfcSetting = () => {
    if (Platform.OS === "android") {
      NfcManager.goToNfcSetting()
        .then(result => {
          console.log("goToNfcSetting OK", result);
        })
        .catch(error => {
          console.warn("goToNfcSetting fail", error);
        });
    }
  };

  _onTagDiscovered = tag => {
    console.log("Tag Discovered", tag);
    this.setState({ tag });
    this._stopDetection();
    this.userLoginByTrhId();
  };

  _stopDetection = () => {
    NfcManager.unregisterTagEvent()
      .then(result => {
        console.log("unregisterTagEvent OK", result);
      })
      .catch(error => {
        console.warn("unregisterTagEvent fail", error);
      });
  };
}

export default LoginPage;

const styles = StyleSheet.create({
  loginView: {
    marginTop: 30,
    width: "90%",
    borderTopWidth: 2,
    borderRightWidth: 2,
    borderBottomWidth: 2,
    borderTopRightRadius: 45,
    borderBottomRightRadius: 50,
    borderColor: "#ccc",
    shadowColor: "#ccc",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1.5
  },
  text: {
    color: "#ddd"
  },
  formInput: {
    padding: 10,
    width: "90%",
    height: "50%",
    justifyContent: "center",
    color: "#ccc",
    textAlign: "center",
    fontSize: 20,
    fontStyle: "italic"
  },
  inputPasword: {
    borderTopWidth: 1,
    borderColor: "#ccc"
  },
  loginBtn: {
    position: "absolute",
    top: "20%",
    right: "-8%",
    backgroundColor: "#2cc8c5",
    width: 80,
    height: 80,
    borderRadius: 50
  },
  loginBtnImage: {
    position: "absolute",
    width: 50,
    height: 50,
    top: 13,
    left: 14
  },
  warningInfo: {
    fontSize: 14,
    color: "red",
    padding: 5
  }
});
