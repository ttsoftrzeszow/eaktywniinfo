import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    AsyncStorage
} from "react-native";

class EAktywniOccupancyInfo extends Component {
    state = {
        occupancy: "0",
        API_URL: "",
        deviceName: ""
    };

    componentDidMount = () => {
        AsyncStorage.getItem("apiUrl").then(apiUrl => {
            this.setState({ API_URL: apiUrl });
            AsyncStorage.getItem("deviceName").then(deviceName => {
                this.setState({ deviceName });
                this.getOccupancyInfo(apiUrl, deviceName);
            });

        });
    };

    getOccupancyInfo = (apiUrl, deviceName) => {
        const url = `${apiUrl}/EAktywni/admin/occupancy`;
        fetch(url, {
            headers: {
                'X-Auth-App-Token': deviceName
            }
        })
            .then(response => {
                if (response.ok) {
                    return response;
                }
                throw Error("Nie udało się");
            })
            .then(response => response.json())
            .then(data => {
                if (!data.isError) {
                    this.setState({
                        occupancy: data.result.amount
                    });
                }
            })
            .catch(err => {
                console.log(err);
            });
    };

    render() {
        return (
            <ScrollView>
                <View style={styles.containerOccupancy}>
                    <View>
                        <Text style={styles.occupancyInfoText}>
                            Ilość osób na obiekcie:
                        </Text>
                    </View>
                    <View>
                        <Text style={styles.occupancyInfoTextNumber}>
                            {this.state.occupancy}
                        </Text>
                    </View>

                </View>
                <Text>{this.state.message}</Text>
            </ScrollView>
        );
    }
}

export default EAktywniOccupancyInfo;

const styles = StyleSheet.create({
    containerOccupancy: {
        flex: 1,
        marginTop: 100,
    },
    occupancyInfoText: {
        fontSize: 26,
        color: "#fff"
    },
    occupancyInfoTextNumber: {
        fontWeight: "bold",
        color: "#f42363",
        fontSize: 90,
        textAlign: "center"
    }
});
