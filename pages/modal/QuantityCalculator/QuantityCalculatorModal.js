import React, { Component } from "react";
import {
  Modal,
  Text,
  TouchableHighlight,
  View,
  StyleSheet, TextInput
} from "react-native";

class QuantityCalculatorModal extends Component {
  state = {
    modalVisible: false,
    qnt: ""
  };

  componentDidMount = () => {
    this.setState({modalVisible: this.props.visible})
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.visible !== this.props.visible) {
      this.setState({modalVisible: this.props.visible})
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
        >
          <View style={styles.containerModal}>

            <TextInput
              style={styles.formInput}
              value={this.state.qnt}
              onChangeText={qnt => this.setState({ qnt })}
              placeholder="Podaj ilość"
              placeholderTextColor="#ccc"
              keyboardType={"number-pad"}
              autoFocus={true}
            />

            <View style={styles.filterViewBtn}>
              <TouchableHighlight
                onPress={() => {
                  this.props.addAssortToBasket(this.state.qnt);
                }}
                style={styles.filterBtn}
              >
                <Text style={styles.textBtn}>OK</Text>
              </TouchableHighlight>
            </View>

          </View>
        </Modal>



      </View>
    );
  }
}

export default QuantityCalculatorModal;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  containerModal: {
    flex: 1,
    marginTop: 22,
    justifyContent: "center",
    alignItems: "center"
  },
  filterBtn: {
    backgroundColor: "#4981d5",
    width: "100%",
    height: 80,
    borderRadius: 10,
  },
  textBtn: {
    color: "#fff",
    textAlign: "center",
    lineHeight: 80,
    fontSize: 30
  },
  filterViewBtn: {
    width: "80%",
    borderWidth: 2,
    borderColor: "transparent"
  },
  formInput: {
    padding: 10,
    width: "90%",
    borderBottomWidth: 1,
    borderColor: "#ccc",
    justifyContent: "center",
    color: "#ccc",
    textAlign: "center",
    fontSize: 20,
    fontStyle: "italic"
  },
});
