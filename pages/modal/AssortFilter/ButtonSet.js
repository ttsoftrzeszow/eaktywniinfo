import React, { Component } from "react";
import {
  Text,
  TouchableHighlight,
  View,
  StyleSheet
} from "react-native";

class ButtonSet extends Component {
  state = {
  };

  handleVisibleModal = (visible) => {
    this.props.onModalVisible(visible);
  }
  render() {
    const {group, shape, color} = this.props;
    return (
      <>

        <View style={group ? styles.filterViewBtnActive : styles.filterViewBtn}>
          <TouchableHighlight
            onPress={() => {
              this.props.onGroups(true);
            }}
            style={group ? styles.filterBtnActive : styles.filterBtn}
          >
            <Text style={group ? styles.textBtnActive : styles.textBtn}>{group ? group.name : "Grupy"}</Text>
          </TouchableHighlight>
        </View>

        <View style={color ? styles.filterViewBtnActive : styles.filterViewBtn}>
          <TouchableHighlight
            onPress={() => {
              this.props.onColors(true);
            }}
            style={color ? styles.filterBtnActive : styles.filterBtn}
          >
            <Text style={color ? styles.textBtnActive : styles.textBtn}>{color ? color.name : "Kolory"}</Text>
          </TouchableHighlight>
        </View>

        <View style={shape ? styles.filterViewBtnActive : styles.filterViewBtn}>
          <TouchableHighlight
            onPress={() => {
              this.props.onShapes(true);
            }}
            style={shape ? styles.filterBtnActive : styles.filterBtn}
          >
            <Text style={shape ? styles.textBtnActive : styles.textBtn}>{shape ? shape.name : "Kształty"}</Text>
          </TouchableHighlight>
        </View>

        <View style={[styles.filterViewBtnActive, {marginTop: 20}]}>
          <TouchableHighlight
            style={styles.filterBtnActive}
            onPress={() => {
              this.handleVisibleModal(false);
            }}
          >
            <Text style={styles.textBtnActive}>Filtruj</Text>
          </TouchableHighlight>
        </View>

        <View style={[styles.filterViewBtnActive, {marginTop: 20}]}>
          <TouchableHighlight
            style={styles.clearBtn}
            onPress={() => {
              this.props.onClearFilter();
            }}
          >
            <Text style={styles.textBtnActive}>Wyczyść</Text>
          </TouchableHighlight>
        </View>

      </>
    );
  }
}

export default ButtonSet;

const styles = StyleSheet.create({
  filterBtn: {
    backgroundColor: "#FFF",
    width: "100%",
    height: 80,
    borderRadius: 10,
  },
  filterBtnActive: {
    backgroundColor: "#4981d5",
    width: "100%",
    height: 80,
    borderRadius: 10,
  },
  clearBtn: {
    backgroundColor: "#ff1a1a",
    width: "100%",
    height: 80,
    borderRadius: 10,
  },
  textBtnActive: {
    color: "#fff",
    textAlign: "center",
    lineHeight: 80,
    fontSize: 30
  },
  textBtn: {
    color: "#4981d5",
    textAlign: "center",
    lineHeight: 80,
    fontSize: 30
  },
  filterViewBtn: {
    width: "80%",
    borderWidth: 2,
    borderColor: "#4981d5",
    borderRadius: 10
  },
  filterViewBtnActive: {
    width: "80%",
    borderWidth: 2,
    borderColor: "transparent",

  }
});
