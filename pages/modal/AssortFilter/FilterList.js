import React, { Component } from "react";
import {
  View,
  StyleSheet, TouchableOpacity, Text, ScrollView
} from "react-native";

class FilterList extends Component {
  state = {

  };

  render() {
    const filterBtn = this.props.filterList.map(item => (
      <View key={item.name} style={styles.btn}>
        <TouchableOpacity
          style={styles.assortBtn}
          onPress={() => this.props.setIds(item, this.props.filterName)}
        >
          <Text style={{ color: "#fff", textAlign: "center", padding: 5 }}>
            {item.name}
          </Text>
        </TouchableOpacity>
      </View>
    ));

    return (
      <ScrollView>
        <View style={styles.container}>
          {filterBtn}
        </View>
      </ScrollView>
    );
  }

}

export default FilterList;

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    flexWrap: "wrap",
    width: "100%"
  },
  assortBtn: {
    backgroundColor: "#4981d5",
    width: "100%",
    height: 50,
    alignItems: "center",
    fontSize: 20,
    textAlign: "center",
    borderRadius: 5
  },
  btn: {
    width: "40%",
    marginTop: 10,
    borderWidth: 2,
    borderColor: "transparent"
  }
});
