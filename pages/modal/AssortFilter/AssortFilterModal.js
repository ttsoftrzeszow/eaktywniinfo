import React, { Component } from "react";
import {
  Modal,
  Text,
  TouchableHighlight,
  View,
  StyleSheet
} from "react-native";
import ButtonSet from "./ButtonSet";
import FilterList from "./FilterList";

class AssortFilterModal extends Component {
  state = {
    modalVisible: false,
    group: null,
    color: null,
    shape: null,
    groupsVisible: false,
    colorsVisible: false,
    shapesVisible: false,
    groupsList: [],
    colorsList: [],
    shapesList: []
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
    if (!visible) {
      this.props.onFilterAssort(this.state.group, this.state.color, this.state.shape);
    }
  }

  getGroupsList = (groupsVisible) => {
    this.setState({groupsVisible});
    this.getFilterList("groups");
  }

  getColorsList = (colorsVisible) => {
    this.setState({colorsVisible});
    this.getFilterList("colors");
  }

  getShapesList = (shapesVisible) => {
    this.setState({shapesVisible});
    this.getFilterList("shapes");
  }

  setIds = (item, filter) => {
    this.setItemFilter(item, filter)
    this.setFilterVisible(false, filter);
  }

  clearFilter = () => {
    this.setState({
      group: null,
      color: null,
      shape: null,
      groupsVisible: false,
      colorsVisible: false,
      shapesVisible: false,
      groupsList: [],
      colorsList: [],
      shapesList: []
    })
  }

  render() {
    const {groupsVisible, colorsVisible, shapesVisible} = this.state;
    return (
      <View style={styles.container}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
        >
          <View style={styles.containerModal}>

            {!groupsVisible && !colorsVisible  && !shapesVisible &&
              <ButtonSet onModalVisible={(visible) => this.setModalVisible(visible)}
                         onGroups={(groupsVisible) => this.getGroupsList(groupsVisible)}
                         onColors={(colorsVisible) => this.getColorsList(colorsVisible)}
                         onShapes={(shapesVisible) => this.getShapesList(shapesVisible)}
                         group={this.state.group}
                         color={this.state.color}
                         shape={this.state.shape}
                         onClearFilter={() => this.clearFilter()}
              />
            }

            {groupsVisible && <FilterList filterList={this.state.groupsList} setIds={(item, filterName) => this.setIds(item, filterName)} filterName={"groups"}/>}
            {colorsVisible && <FilterList filterList={this.state.colorsList} setIds={(item, filterName) => this.setIds(item, filterName)} filterName={"colors"}/>}
            {shapesVisible && <FilterList filterList={this.state.shapesList} setIds={(item, filterName) => this.setIds(item, filterName)} filterName={"shapes"}/>}

          </View>
        </Modal>

        <View style={styles.filterViewBtn}>
          <TouchableHighlight
            onPress={() => {
              this.setModalVisible(true);
            }}
            style={styles.filterBtn}
          >
            <Text style={styles.textBtn}>Filtry</Text>
          </TouchableHighlight>
        </View>

      </View>
    );
  }

  getFilterList = (filtr) => {
    fetch(this.getUrlFilter(filtr))
      .then(response => {
        if (response.ok) {
          return response;
        }
        throw Error("Nie udało się");
      })
      .then(response => response.json())
      .then(data => {
        if (!data.isError) {
          console.log(data);
          this.setFilterData(data, filtr);
        } else {
          if (data.errorTable.length > 0) {
            alert(data.errorTable[0]);
          } else {
            alert("Nieznany błąd");
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  getUrlFilter = (filtr): string => {
    if (filtr === 'groups') {
      return `${this.props.apiUrl}/crud/materialGroup`;
    } else if (filtr === 'colors') {
      return `${this.props.apiUrl}/crud/materialColor`;
    } else if (filtr === "shapes") {
      return `${this.props.apiUrl}/crud/shape`;
    } else {
      return "";
    }
  }

  setFilterData = (data, filtr): any => {
    if (filtr === 'groups') {
      this.setState({
        groupsList: data.result
      });
    } else if (filtr === 'colors') {
      this.setState({
        colorsList: data.result
      });
    } else if (filtr === "shapes") {
      this.setState({
        shapesList: data.result
      });
    } else {
      this.setState({
        groupsList: [],
        colorsList: [],
        shapesList: []
      });
    }
  }

  setFilterVisible = (visible, filtr) => {
    if (filtr === 'groups') {
      this.setState({
        groupsVisible: visible
      });
    } else if (filtr === 'colors') {
      this.setState({
        colorsVisible: visible
      });
    } else if (filtr === "shapes") {
      this.setState({
        shapesVisible: visible
      });
    } else {
      this.setState({
        groupsVisible: false,
        colorsVisible: false,
        shapesVisible: false
      });
    }
  }

  setItemFilter = (item, filtr) => {
    if (filtr === 'groups') {
      this.setState({
        group: item
      });
    } else if (filtr === 'colors') {
      this.setState({
        color: item
      });
    } else if (filtr === "shapes") {
      this.setState({
        shape: item
      });
    } else {
      this.setState({
        group: null,
        color: null,
        shape: null
      });
    }
  }
}

export default AssortFilterModal;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  containerModal: {
    flex: 1,
    marginTop: 22,
    justifyContent: "center",
    alignItems: "center"
  },
  saveBtn: {
    textAlign: "center",
    lineHeight: 40,
    marginTop: 20,
    backgroundColor: "#2cc8c5",
    width: 150,
    height: 40,
    borderRadius: 10,
    color: "white"
  },
  filterBtn: {
    backgroundColor: "#4981d5",
    width: "100%",
    height: 80,
    borderRadius: 10,
  },
  textBtn: {
    color: "#fff",
    textAlign: "center",
    lineHeight: 80,
    fontSize: 30
  },
  filterViewBtn: {
    width: "80%",
    borderWidth: 2,
    borderColor: "transparent"
  }
});
