import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Button,
  FlatList,
  AsyncStorage
} from "react-native";

class WristletInfo extends Component {
  state = {
    billDetails: "",
    trhId: "",
    details: false,
    error: "",
    API_URL: ""
  };

  componentDidMount = () => {
    AsyncStorage.getItem("apiUrl").then(apiUrl => {
      this.setState({ API_URL: apiUrl });
      trhId = this.props.match.params.trhId;
      this.refreshOpenBIll(trhId, apiUrl);
    });
  };

  refreshOpenBIll = (trhId, api) => {
    AsyncStorage.getItem("deviceName").then(deviceName => {
      const url = `${api}/bill/refresh?trhId=${trhId}&singleSettlement=true&ip=${deviceName}`;
      fetch(url)
        .then(response => {
          if (response.ok) {
            console.log(response.ok);
            return response;
          }
          throw Error("Nie udało się");
        })
        .then(response => response.json())
        .then(data => {
          if (!data.isError) {
            this.setState({
              billDetails: data.result,
              error: ""
            });
            this.summarizeBill(trhId, api, deviceName);
          } else {
            this.setState({
              billDetails: "",
              error: data.errorTable[0]
            });
          }
        })
        .catch(err => {
          console.log(err);
        });
    });
  };

  summarizeBill = (trhId, api, deviceName) => {
    const params = {
      billIds: `${this.state.billDetails.objectDb},${this.state.billDetails.objectId}`,
      singleSettlement: true,
      trhId: trhId,
      ipAddress: deviceName
    };
    const url = `${api}/bill/summarize`;
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(params)
    })
      .then(response => {
        if (response.ok) {
          console.log(response.ok);
          return response;
        }
        throw Error("Nie udało się");
      })
      .then(response => response.json())
      .then(data => {
        this.setState({
          billDetails: data.result,
          trhId: data.result.wristlets[0].logo,
          error: ""
        });
        // if (!data.isError) {
        //   console.log(data.result);
        //   this.setState({
        //     billDetails: data.result,
        //     trhId: data.result.wristlets[0].logo,
        //     error: ""
        //   });
        // } else {
        //   this.setState({
        //     billDetails: "",
        //     trhId: "",
        //     error: data.errorTable[0]
        //   });
        // }
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleShowHideDetails = () => {
    this.setState({
      details: !this.state.details
    });
  };

  render() {
    if (!this.state.details) {
      return (
        <>
          <View style={styles.containerWristletInfo}>
            {!!this.state.error && (
              <Text style={styles.errorText}>{this.state.error}</Text>
            )}
            <Text style={styles.wristletInfoText}>
              Pasek:{" "}
              <Text style={{ fontWeight: "bold", fontSize: 50 }}>
                {this.state.trhId}
              </Text>
            </Text>
            <Text style={styles.wristletInfoText}>
              Dopłata:{" "}
              <Text style={{ fontWeight: "bold", fontSize: 50 }}>
                {this.state.billDetails.total}
              </Text>
            </Text>
            <View style={styles.btnDetails}>
              <Button
                title="Szczegóły"
                color="#0AB6CE"
                onPress={this.handleShowHideDetails}
              />
            </View>
          </View>
        </>
      );
    } else {
      return (
        <>
          <View style={styles.containerDetails}>
            <Text style={styles.h2text}>Szczegóły</Text>
            <FlatList
              data={this.state.billDetails.verbose}
              showsVerticalScrollIndicator={false}
              renderItem={({ item }) => (
                <View style={styles.flatview}>
                  <Text style={styles.name}>
                    {item.type}{" "}
                    <Text style={{ fontWeight: "bold", fontSize: 30 }}>
                      {item.place}
                    </Text>
                  </Text>
                  <Text style={styles.valid}>Cena: {item.price}</Text>
                </View>
              )}
              keyExtractor={item => item.place}
            />
          </View>
          <View style={styles.btnReturn}>
            <Button
              title="Powrót"
              color="#0AB6CE"
              onPress={this.handleShowHideDetails}
            />
          </View>
        </>
      );
    }
  }
}

export default WristletInfo;

const styles = StyleSheet.create({
  containerWristletInfo: {
    marginTop: "20%",
    padding: 20
  },
  errorText: {
    color: "red",
    fontSize: 40
  },
  wristletInfoText: {
    color: "#fff",
    fontSize: 40
  },
  btnDetails: {
    marginTop: 50,
    fontSize: 20
  },

  containerDetails: {
    flex: 1,
    marginTop: "20%",
    justifyContent: "center",
    alignItems: "center"
  },
  h2text: {
    marginTop: 10,
    fontFamily: "Helvetica",
    fontSize: 40,
    fontWeight: "bold",
    color: "#fff"
  },
  flatview: {
    justifyContent: "center",
    paddingTop: 30,
    borderRadius: 2
  },
  name: {
    fontFamily: "Verdana",
    fontSize: 23,
    color: "#fff"
  },
  valid: {
    color: "green",
    fontSize: 23
  },
  btnReturn: {
    marginBottom: 30
  }
});
