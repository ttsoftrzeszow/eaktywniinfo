import React from "react";
import { Switch, Route } from "react-router-native";
import ToolbarComponent from "./ToolbarComponent";

const ToolbarNavigate = () => {
  return (
    <Switch>
      <Route
        path="/occupancyInfo"
        render={() => (
          <ToolbarComponent
            title="Zajętość obiektu"
            image={require("../../images/ico2.png")}
            color="#f42363"
          />
        )}
      />
      <Route
        path="/nfc/wristlet"
        render={() => (
          <ToolbarComponent
            title="O pasku"
            image={require("../../images/ico1.png")}
            color="#ffa736"
          />
        )}
      />
      <Route
        path="/nfc/accountInfo"
        render={() => (
          <ToolbarComponent
            title="Konto klienta"
            image={require("../../images/ico3.png")}
            color="#2cc884"
          />
        )}
      />
      <Route
        path="/accountInfo/:trhId"
        render={() => (
          <ToolbarComponent
            title="Konto klienta"
            image={require("../../images/ico3.png")}
            color="#2cc884"
          />
        )}
      />
      <Route
        path="/wristlet/:trhId"
        render={() => (
          <ToolbarComponent
            title="O pasku"
            image={require("../../images/ico1.png")}
            color="#ffa736"
          />
        )}
      />
      <Route
        path="/rent"
        render={() => (
          <ToolbarComponent
            title="Wypożyczalnia"
            image={require("../../images/ico4.png")}
            color="#4981d5"
          />
        )}
      />
      <Route
        path="/rent/:trhId"
        render={() => (
          <ToolbarComponent
            title="Wypożyczalnia"
            image={require("../../images/ico4.png")}
            color="#4981d5"
          />
        )}
      />
      <Route
        path="/notifications"
        render={() => (
          <ToolbarComponent
            title="Alerty"
            image={require("../../images/ico2.png")}
            color="#f42363"
          />
        )}
      />
      <Route
        path="/store"
        render={() => (
          <ToolbarComponent
            title="Stan magazynu"
            image={require("../../images/ico2.png")}
            color="#f42363"
          />
        )}
      />
      <Route
        path="/open/lockers"
        render={() => (
          <ToolbarComponent
            title="Otwarcie szafki"
            image={require("../../images/ico4.png")}
            color="#4981d5"
          />
        )}
      />
      <Route
        path="/nfc/ticketInfo"
        render={() => (
          <ToolbarComponent
            title="Bilet"
            image={require("../../images/ico1.png")}
            color="#ffa736"
          />
        )}
      />
      <Route
        path="/ticketInfo/:trhId"
        render={() => (
          <ToolbarComponent
            title="Bilet"
            image={require("../../images/ico1.png")}
            color="#ffa736"
          />
        )}
      />
      <Route
        path="/camera/ticket/info"
        render={() => (
          <ToolbarComponent
            title="Info bilet"
            image={require("../../images/ico1.png")}
            color="#ffa736"
          />
        )}
      />
      <Route
        path="/eaktywni/occupancy/info"
        render={() => (
          <ToolbarComponent
            title="Wpuszczone bilety"
            image={require("../../images/ico4.png")}
            color="#4981d5"
          />
        )}
      />
      <Route
        path="/epos"
        render={() => (
          <ToolbarComponent
            title="BAR"
            image={require("../../images/ico4.png")}
            color="#4981d5"
          />
        )}
      />
      <Route
        path="/basket"
        render={() => (
          <ToolbarComponent
            title="Koszyk"
            image={require("../../images/ico4.png")}
            color="#4981d5"
          />
        )}
      />
      <Route
        path="/tickets"
        render={() => (
          <ToolbarComponent
            title="Bilety"
            image={require("../../images/ico4.png")}
            color="#4981d5"
          />
        )}
      />
      <Route
        path="/zoneInfo"
        render={() => (
          <ToolbarComponent
            title="Wybierz strefę"
            image={require("../../images/ico2.png")}
            color="#f42363"
          />
        )}
      />
    </Switch>
  );
};

export default ToolbarNavigate;
