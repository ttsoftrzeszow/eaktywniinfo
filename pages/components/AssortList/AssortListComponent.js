import React, { Component } from "react";
import {
  View,
  StyleSheet, TouchableOpacity, Text
} from "react-native";
import AssortFilterModal from "../../modal/AssortFilter/AssortFilterModal";

class AssortListComponent extends Component {
  state = {

  };

  render() {
    const assortRent = this.props.assortList.map(item => (
      <View key={item.name} style={styles.btn}>
        <TouchableOpacity
          style={styles.assortBtn}
          onPress={() => this.props.selectAssort(item)}
        >
          <Text style={{ color: "#fff", textAlign: "center", padding: 5 }}>
            {item.name}
          </Text>
          <Text style={{ color: "#fff", textAlign: "center" }}>
            Cena: {item.price}
          </Text>
        </TouchableOpacity>
      </View>
    ));

    return (
      <>
        <AssortFilterModal apiUrl={this.props.apiUrl} onFilterAssort={this.props.onFilterAssort}/>

        <View style={styles.container}>
          {assortRent}
        </View>
      </>
    );
  }

}

export default AssortListComponent;

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    flexWrap: "wrap",
    width: "100%"
  },
  assortBtn: {
    backgroundColor: "#4981d5",
    width: "100%",
    height: 80,
    alignItems: "center",
    fontSize: 20,
    textAlign: "center",
    borderRadius: 5
  },
  btn: {
    width: "40%",
    marginTop: 20,
    borderWidth: 2,
    borderColor: "transparent"
  }
});
