import React, { Component } from "react";
import {
  Text,
  StyleSheet, View, TouchableOpacity, AsyncStorage
} from "react-native";

class FooterComponent extends Component {
  state = {
  };

  render() {
    return (
      <View style={[styles.container, , { backgroundColor: "#4981d5" }]}>
        <View style={styles.toolbarContainer}>
          <Text style={styles.toolbarText}>Ilość: {this.props.basketLength}</Text>

          <Text style={styles.toolbarText}>Suma: {this.props.basketPrice}</Text>

          <TouchableOpacity onPress={() => this.props.onNext()} style={styles.menuBtn}>
            <Text
              style={{
                color: "#fff",
                fontSize: 20,
                lineHeight: 60
              }}
            >
              DALEJ
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default FooterComponent;

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    bottom: 0,
    left: 0,
    width: "100%",
    height: 60,
    elevation: 4
  },
  toolbarContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginHorizontal: 8,
    height: 56,
    flex: 1
  },
  toolbarText: {
    fontSize: 20,
    color: "#fff"
  },
  menuBtn: {
    height: "100%",
    width: 70,
    alignItems: "center",
    marginRight: 10,
    zIndex: 1000
  }
});
