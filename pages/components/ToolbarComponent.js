import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  BackHandler
} from "react-native";
import { Redirect } from "react-router-native";

class ToolbarComponent extends Component {
  state = {
    redirect: false
  };

  setRedirect = () => {
    this.setState({
      redirect: true
    });
    return true;
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/" />;
    }
  };

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.setRedirect);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.setRedirect);
  }

  render() {
    return (
      <View style={[styles.container, , { backgroundColor: this.props.color }]}>
        {this.renderRedirect()}
        <View style={styles.toolbarContainer}>
          <ImageBackground
            source={this.props.image}
            style={{
              width: 40,
              height: 40,
              alignItems: "center",
              justifyContent: "center"
            }}
          />

          <Text style={styles.toolbarText}>{this.props.title}</Text>

          <TouchableOpacity onPress={this.setRedirect} style={styles.menuBtn}>
            <Text
              style={{
                color: this.props.color,
                fontSize: 20,
                lineHeight: 60
              }}
            >
              MENU
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default ToolbarComponent;

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: 60,
    elevation: 4
  },
  toolbarContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginHorizontal: 8,
    height: 56,
    flex: 1
  },
  toolbarText: {
    fontSize: 20,
    color: "#fff"
  },
  menuBtn: {
    height: "100%",
    width: 70,
    alignItems: "center",
    marginRight: 10,
    zIndex: 1000
  }
});
