import React, { Component } from "react";
import {
  View,
  Text,
  Button,
  StyleSheet,
  ScrollView,
  FlatList,
  AsyncStorage
} from "react-native";

class AccountPage extends Component {
  state = {
    account: "",
    passPage: false,
    error: "",
    API_URL: ""
  };
  componentDidMount = () => {
    AsyncStorage.getItem("apiUrl").then(apiUrl => {
      this.setState({ API_URL: apiUrl });
      trhId = this.props.match.params.trhId;
      this.getAccountData(trhId, apiUrl);
    });
  };

  getAccountData = (id, api) => {
    const params = {
      bioIds: id
    };
    const url = `${api}/crud/account/byCard/data`;
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(params)
    })
      .then(response => {
        if (response.ok) {
          console.log(response.ok);
          return response;
        }
        throw Error("Nie udało się");
      })
      .then(response => response.json())
      .then(data => {
        if (!data.isError) {
          if (data.result.fullAccount.objectId !== 0) {
            this.setState({
              account: data.result.fullAccount,
              error: ""
            });
          } else {
            this.setState({
              error: "Brak przypisanego konta do tej karty"
            });
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleShowHidePass = () => {
    this.setState({
      passPage: !this.state.passPage
    });
  };

  render() {
    const {
      holderData1,
      openDateCaption,
      closeDateCaption,
      balanceI,
      balanceII,
      balanceIII,
      depositBalance,
      closeDate
    } = this.state.account;

    let isValid = false;
    const today = new Date().getTime();
    if (closeDate > today) {
      isValid = true;
    }

    if (!this.state.passPage) {
      if (this.state.error) {
        return (
          <View style={styles.containerPass}>
            <Text style={styles.h2textError}>{this.state.error}</Text>
          </View>
        );
      } else {
        return (
          <ScrollView>
            <View style={styles.accountContainer}>
              <Text style={styles.accountName}>
                Konto: <Text style={styles.accountNameText}>{holderData1}</Text>
              </Text>
              <Text
                style={
                  isValid
                    ? [styles.accountValidate, { color: "green" }]
                    : [styles.accountValidate, { color: "red" }]
                }
              >
                Ważne od: {openDateCaption} do: {closeDateCaption}
              </Text>
              <View style={styles.accountDeposit}>
                <Text style={styles.accountDepositText}>
                  Saldo I: {balanceI}
                </Text>
                <Text style={styles.accountDepositText}>
                  Saldo II: {balanceII}
                </Text>
                <Text style={styles.accountDepositText}>
                  Saldo III: {balanceIII}
                </Text>
                <Text style={styles.accountDepositText}>
                  Kaucja: {depositBalance}
                </Text>
              </View>
            </View>
            <View>
              <Button
                title="Karnety"
                color="#0AB6CE"
                onPress={this.handleShowHidePass}
              />
            </View>
          </ScrollView>
        );
      }
    } else {
      return (
        <>
          <View style={styles.containerPass}>
            <Text style={styles.h2text}>Karnety</Text>
            <FlatList
              data={this.state.account.passList}
              showsVerticalScrollIndicator={false}
              renderItem={({ item }) => (
                <View style={styles.flatview}>
                  <Text style={styles.name}>{item.name}</Text>
                  <Text style={styles.valid}>
                    Ważny od: {item.validfrom} do: {item.validto}
                  </Text>
                </View>
              )}
              keyExtractor={item => item.objectId}
            />
          </View>
          <View style={styles.btnReturn}>
            <Button
              title="Powrót"
              color="#0AB6CE"
              onPress={this.handleShowHidePass}
            />
          </View>
        </>
      );
    }
  }
}

export default AccountPage;

const styles = StyleSheet.create({
  accountContainer: {
    marginTop: "20%",
    padding: 20
  },
  accountName: {
    color: "white",
    fontSize: 20
  },
  accountNameText: {
    color: "white",
    fontSize: 30,
    fontWeight: "bold"
  },
  accountValidate: {
    marginTop: 20,
    marginLeft: "auto",
    marginRight: "auto",
    fontSize: 15
  },
  accountDeposit: {
    marginTop: 20,
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-around"
  },
  accountDepositText: {
    width: "100%",
    color: "#fff",
    fontSize: 30,
    borderBottomWidth: 1,
    borderBottomColor: "#ccc"
  },
  btnCarnet: {
    marginTop: "20%"
  },
  containerPass: {
    flex: 1,
    marginTop: "20%",
    justifyContent: "center",
    alignItems: "center"
  },
  h2text: {
    marginTop: 10,
    fontFamily: "Helvetica",
    fontSize: 36,
    fontWeight: "bold",
    color: "#fff"
  },
  h2textError: {
    marginTop: 10,
    fontFamily: "Helvetica",
    fontSize: 26,
    fontWeight: "bold",
    color: "red"
  },
  flatview: {
    justifyContent: "center",
    paddingTop: 30,
    borderRadius: 2
  },
  name: {
    fontFamily: "Verdana",
    fontSize: 18,
    color: "#fff"
  },
  valid: {
    color: "red"
  },
  btnReturn: {
    marginBottom: 30
  },
  notPass: {
    color: "red"
  }
});
