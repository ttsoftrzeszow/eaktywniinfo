import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Button,
  FlatList,
  AsyncStorage,
  TouchableOpacity
} from "react-native";

class StorePage extends Component {
  state = {
    stores: [],
    selectStore: "",
    billLine: [],
    API_URL: ""
  };

  apiUrl = this.props.apiUrl;

  componentDidMount() {
    AsyncStorage.getItem("apiUrl").then(apiUrl => {
      this.setState({ API_URL: apiUrl });
      this.getStores(apiUrl);
    });
  }

  getStores = apiUrl => {
    const url = `${apiUrl}/crud/store`;
    fetch(url)
      .then(response => {
        if (response.ok) {
          return response;
        }
        throw Error("Nie udało się");
      })
      .then(response => response.json())
      .then(data => {
        if (!data.isError) {
          console.log(data.result);
          this.setState({
            stores: data.result
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  getStoreCurrentQnt = store => {
    let storeIds = `${store.objectDb},${store.objectId}`;
    const url = `${
      this.state.API_URL
    }/crud/store/currentQuantity?storeIds=${storeIds}`;
    fetch(url)
      .then(response => {
        if (response.ok) {
          return response;
        }
        throw Error("Nie udało się");
      })
      .then(response => response.json())
      .then(data => {
        if (!data.isError) {
          console.log(data.result);
          this.setState({
            billLine: data.result
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleSelectStore = store => {
    this.setState({
      selectStore: store
    });
    this.getStoreCurrentQnt(store);
  };

  handleReturnStorePage = () => {
    this.setState({
      selectStore: "",
      billLine: []
    });
  };

  render() {
    if (this.state.selectStore) {
      return (
        <ScrollView>
          <View style={{ marginTop: 70 }}>
            <FlatList
              data={this.state.billLine}
              showsVerticalScrollIndicator={false}
              renderItem={({ item }) => (
                <View style={styles.flatview}>
                  <Text style={styles.name}>{`${item.name} (${item.objectDb}, ${
                    item.objectId
                  })`}</Text>
                  <Text style={{ color: "#fff" }}>Ilość: {item.qnt}</Text>
                </View>
              )}
              keyExtractor={item => `${item.objectId}o`}
            />
            <View style={{ marginTop: 30 }}>
              <Button
                title="Powrót"
                color="#0AB6CE"
                onPress={this.handleReturnStorePage}
              />
            </View>
          </View>
        </ScrollView>
      );
    } else {
      const storeList = this.state.stores.map(item => (
        <View key={item.objectId} style={styles.menuBtn}>
          <TouchableOpacity
            style={styles.storeBtn}
            onPress={() => this.handleSelectStore(item)}
          >
            <Text
              style={{
                color: "#fff",
                textAlign: "center",
                padding: 5,
                height: 50
              }}
            >
              {item.storeName}
            </Text>
          </TouchableOpacity>
        </View>
      ));
      return (
        <ScrollView>
          <View style={styles.containerStore}>
            <Text style={[styles.textStore, { textAlign: "center" }]}>
              Magazyny
            </Text>
            {storeList}
          </View>
        </ScrollView>
      );
    }
  }
}

export default StorePage;

const styles = StyleSheet.create({
  containerStore: {
    flex: 1,
    marginTop: 90,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    flexWrap: "wrap",
  },
  textStore: {
    width: "100%",
    fontFamily: "Helvetica",
    fontSize: 20,
    fontWeight: "bold",
    color: "#fff"
  },
  storeBtn: {
    backgroundColor: "#0AB6CE",
    width: "100%",
    height: 50,
    alignItems: "center",
    fontSize: 20,
    textAlign: "center",
    borderRadius: 5
  },
  textBtn: {
    position: "absolute",
    bottom: 0,
    color: "#fff"
  },
  menuBtn: {
    width: "40%",
    marginTop: 20
  },
  flatview: {
    justifyContent: "center",
    padding: 30,
    borderBottomWidth: 1,
    borderColor: "#ccc"
  },
  name: {
    fontFamily: "Verdana",
    fontSize: 18,
    color: "#fff"
  }
});
