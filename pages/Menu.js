import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  ScrollView,
  Button,
  BackHandler,
  Alert
} from "react-native";
import { Link } from "react-router-native";

const menuList = [
  // {
  //   name: "Zajętość obiektu",
  //   path: "/occupancyInfo",
  //   img: require("../images/btn-red.jpg")
  // },
  // {
  //   name: "O pasku",
  //   path: "/nfc/wristlet",
  //   img: require("../images/btn-orange.jpg")
  // },
  {
    name: "Skaner",
    path: "/nfc/ticketInfo",
    img: require("../images/btn-orange.jpg")
  },
  {
    name: "Aparat",
    path: "/camera/ticket/info",
    img: require("../images/btn-blue.jpg")
  },
  // {
  //   name: "Konto klienta",
  //   path: "/nfc/accountInfo",
  //   img: require("../images/btn-green.jpg")
  // },
  // {
  //   name: "Wypożyczalnia",
  //   path: "/rent",
  //   img: require("../images/btn-blue.jpg")
  // },
  // {
  //   name: "Alerty",
  //   path: "/notifications",
  //   img: require("../images/btn-red.jpg")
  // },
  // {
  //   name: "Stan magazynu",
  //   path: "/store",
  //   img: require("../images/btn-red.jpg")
  // },
  // {
  //   name: "Otwarcie szafki",
  //   path: "/open/lockers",
  //   img: require("../images/btn-blue.jpg")
  // },
  {
    name: "Wpuszczone bilety",
    path: "/eaktywni/occupancy/info",
    img: require("../images/btn-blue.jpg")
  },
  // {
  //   name: "BAR",
  //   path: "/epos",
  //   img: require("../images/btn-blue.jpg")
  // },
  {
    name: "Bilety",
    path: "/tickets",
    img: require("../images/btn-blue.jpg")
  },
  {
    name: "Wybierz strefę",
    path: "/zoneInfo",
    img: require("../images/btn-red.jpg")
  },
];

class Menu extends Component {
  state = {};

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", () => {
      Alert.alert(
        "Potwierdź wyjście",
        "Czy chcesz wyjść z aplikacji?",
        [
          { text: "ANULUJ", style: "cancel" },
          { text: "OK", onPress: () => BackHandler.exitApp() }
        ],
        { cancelable: false }
      );
      return true;
    });
  }

  render() {
    const menu = menuList.map(item => (
      <View key={item.name} style={styles.menuBtn}>
        <ImageBackground source={item.img} style={styles.imageBtn}>
          <Link to={item.path} style={styles.linkBtn}>
            <Text style={styles.textBtn}>{item.name}</Text>
          </Link>
        </ImageBackground>
      </View>
    ));

    return (
      <ImageBackground
        source={require("../images/bck-menu.jpg")}
        style={{
          width: "100%",
          height: "100%",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <ScrollView>
          <View style={styles.container}>{menu}</View>
        </ScrollView>
        <View style={styles.logiText}>
          <Text>
            Zalogowany jako:{" "}
            <Text style={{ fontWeight: "bold" }}>{this.props.operator}</Text>
          </Text>
        </View>
        <View style={styles.btnLogout}>
          <Button onPress={this.props.logout} title="Wyloguj" color="#2cc8c5" />
        </View>
      </ImageBackground>
    );
  }
}

export default Menu;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    flexWrap: "wrap",
    marginTop: 50,
    width: "95%"
    // padding: 20
  },
  menuBtn: {
    marginTop: 20,
    width: "40%",
    height: 135
  },
  imageBtn: {
    width: 100,
    height: 100
  },
  linkBtn: {
    // width: "100%",
    height: 135,
    alignItems: "center"
  },
  textBtn: {
    position: "absolute",
    bottom: 0,
    color: "#000"
  },
  logiText: {
    position: "absolute",
    left: 10,
    top: 10
  },
  btnLogout: {
    position: "absolute",
    right: 20,
    top: 10
  }
});
