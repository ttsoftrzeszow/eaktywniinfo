import React, { Component } from "react";
import {
  View,
  StyleSheet, ScrollView, Text, TouchableHighlight, AsyncStorage
} from "react-native";
import { Redirect } from "react-router-native";

class BasketPage extends Component {
  state = {
    redirectMenu: false,
    redirectFinalize: false
  };


  handleRedirectMEnuPage = () => {
    this.setState({
      redirectMenu: true
    });
    return true;
  }

  handleRedirectFinalizePage = () => {
    this.setState({
      redirectFinalize: true
    });
    return true;
  }

  renderRedirect = () => {
    if (this.state.redirectMenu) {
      return <Redirect to={"/"} />;
    }
    if (this.state.redirectFinalize) {
      return <Redirect to={{pathname: "/finalize", state: {saveError: this.state.saveError, saveSuccess: this.state.saveSuccess, errorType: this.state.errorType}}} />;
    }

  };


  render() {
    return (

        <ScrollView >
          <View style={styles.container}>
          {this.renderRedirect()}
          {this.props.location.state.saveError !== "" && <Text style={styles.text}>Wyjatek! {this.props.location.state.saveError}</Text>}
          {this.props.location.state.saveSuccess !== "" && <Text style={styles.text}>Sukces! {this.props.location.state.saveSuccess}</Text>}

            {this.props.location.state.errorType !== "WetBarLimitExceded" &&
              <View style={[styles.payViewBtn, {marginTop: 20}]}>
                <TouchableHighlight
                  style={styles.payBtnActive}
                  onPress={() => {
                    this.handleRedirectMEnuPage();
                  }}
                >
                  <Text style={styles.textBtnActive}>OK</Text>
                </TouchableHighlight>
              </View>
            }

            {this.props.location.state.errorType === "WetBarLimitExceded" &&
              <>
                <View style={[styles.payViewYesBtn]}>
                  <TouchableHighlight
                    style={styles.payBtnYesActive}
                    onPress={() => {
                      this.saveBill(this.props.location.state.trhId);
                    }}
                  >
                    <Text style={styles.textBtnActive}>TAK</Text>
                  </TouchableHighlight>
                </View>

                <View style={[styles.payViewNoBtn]}>
                  <TouchableHighlight
                    style={styles.payBtnNoActive}
                    onPress={() => {
                      this.handleRedirectMEnuPage();
                    }}
                  >
                    <Text style={styles.textBtnActive}>NIE</Text>
                  </TouchableHighlight>
                </View>
              </>



            }

          </View>
        </ScrollView>
    );
  }

  saveBill = (trhId) => {

    AsyncStorage.getItem("apiUrl").then(apiUrl => {
      AsyncStorage.getItem("basket").then(storageBasket => {
        const basket = JSON.parse(storageBasket);
        const lines = [];
        basket.listItem.forEach(function (item) {
          const TicketBillLinePOJO = {
            assort: item.assort,
            quantity: 1,
            ticketAccessList: item.ticketAccessList
          }

          lines.push(TicketBillLinePOJO);
        })

        let TicketBillHolderPOJO = {
          accountPOJO: null,
          purchaser: null,
          payType: 'M',
          documentType: "RA",
          ipAddress: "",
          lines: lines,
          createInvoice: false,
          recipient: null,
          trhId: trhId,
          forceSave: true
        }

        const url = `${apiUrl}/admin_creat_new_bill`;
        fetch(url, {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(TicketBillHolderPOJO)
        })
          .then(response => {
            if (response.ok) {
              return response;
            }
            throw Error("Nie udało się");
          })
          .then(response => response.json())
          .then(data => {
            if (!data.isError) {
              this.setState({saveSuccess: "Transakcja przebiegła pomyślnie"});
              this.handleRedirectFinalizePage();
            } else {
              if (data.errorTable.length  > 0) {
                this.setState({
                  saveError: data.errorTable[0],
                  errorType: data.messageType
                });
              } else {
                this.setState({
                  saveError: "Nieznany błąd",
                  errorType: data.messageType
                });
              }
              this.handleRedirectFinalizePage();
            }
          })
          .catch(err => {
            console.log(err);
          });

      });
    });


  }


}

export default BasketPage;

const styles = StyleSheet.create({
  container: {
    marginTop: 100,
    justifyContent: "center",
    alignItems: "center",
    flexWrap: "wrap",
    width: "100%"
  },
  text: {
    marginTop: 20,
    fontWeight: "bold",
    fontSize: 20,
    color: "#fff"
  },
  payBtnActive: {
    backgroundColor: "#4981d5",
    width: "100%",
    height: 80,
    borderRadius: 10,
  },
  textBtnActive: {
    color: "#fff",
    textAlign: "center",
    lineHeight: 80,
    fontSize: 30
  },
  payViewBtn: {
    width: "80%",
    borderWidth: 2,
    borderColor: "#4981d5",
    borderRadius: 10
  },
  payViewYesBtn: {
    width: "40%",
    borderWidth: 2,
    borderColor: "#33cc33",
    borderRadius: 10
  },
  payViewNoBtn: {
    width: "40%",
    borderWidth: 2,
    borderColor: "#ff0000",
    borderRadius: 10
  },
  payBtnYesActive: {
    backgroundColor: "#33cc33",
    width: "100%",
    height: 80,
    borderRadius: 10,
  },
  payBtnNoActive: {
    backgroundColor: "#ff0000",
    width: "100%",
    height: 80,
    borderRadius: 10,
  },
});
